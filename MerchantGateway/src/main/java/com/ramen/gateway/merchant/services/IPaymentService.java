package com.ramen.gateway.merchant.services;

public interface IPaymentService {
    String makePayment(String paymentInfo);

    String makeRefund(String transactionId);
}
