package com.ramen.gateway.merchant.model;

public class PaymentInfo {
    private String fromAccount;
    private String toAccount;
    private int amount;

    public String getFromAccount() {
        return fromAccount;
    }

    public String getToAccount() {
        return toAccount;
    }

    public int getAmount() {
        return amount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
