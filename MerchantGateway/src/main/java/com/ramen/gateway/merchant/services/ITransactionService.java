package com.ramen.gateway.merchant.services;

public interface ITransactionService {
    String getTransactionReport(String jsonInput);
}
