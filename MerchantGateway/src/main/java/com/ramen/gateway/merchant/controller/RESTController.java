package com.ramen.gateway.merchant.controller;

import com.google.gson.JsonObject;
import com.ramen.gateway.merchant.model.GenericResponse;
import com.ramen.gateway.merchant.services.IMerchantService;
import com.ramen.gateway.merchant.services.IPaymentService;
import com.ramen.gateway.merchant.services.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@CrossOrigin(origins = {"http://localhost:8889", "http://02267-osaka.compute.dtu.dk:8889"}, maxAge = 3600)
@Controller
@RestController
@RequestMapping("merchant")
public class RESTController {

    private final IPaymentService paymentService;

    private final IMerchantService merchantService;

    private final ITransactionService transactionService;

    @Autowired
    public RESTController(IPaymentService paymentService, IMerchantService merchantService, ITransactionService transactionService) {
        this.paymentService = paymentService;
        this.merchantService = merchantService;
        this.transactionService = transactionService;
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> createMerchant(@RequestBody String merchant) {
        return makeResponse(merchantService.createMerchant(merchant));
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     * @param merchant to be updated
     * @return the updated merchant or error
     */
    @PutMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> updateMerchant(@RequestBody String merchant) {
        return makeResponse(merchantService.updateMerchant(merchant));
    }

    /**
     * @author Mathias Asmussen
     * @param id of the merchant to be deleted
     * @return whether the delete was successful
     */
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<String> deleteMerchant(@PathVariable UUID id) {
        return makeResponse(merchantService.deleteMerchant(id.toString()));
    }

    /**
     * @author Alexander Armstrong
     * @param id of the merchant to get
     * @return the merchant object or error
     */
    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity<String> getMerchant(@PathVariable String id) {
        return makeResponse(merchantService.getMerchant(id));
    }

    @PutMapping(consumes = "application/json", path = "/payment", produces = "application/json")
    public ResponseEntity<String> makePayment(@RequestBody String paymentInfo) {
        return makeResponse(paymentService.makePayment(paymentInfo));
    }

    /**
     * Return a list of transactions for that user within the time span
     * @author Mathias E. Asmussen - s154219
     * @param userId
     * @param fromDate in epoch seconds
     * @param toDate in epoch seconds
     * @return
     */
    @GetMapping(path = "/transaction/{userId}/{fromDate}/{toDate}", produces = "application/json")
    public ResponseEntity<String> getTransactions(@PathVariable String userId, @PathVariable String fromDate,
                                                  @PathVariable String toDate) {
        JsonObject transactionInfo = new JsonObject();
        transactionInfo.addProperty("start", fromDate);
        transactionInfo.addProperty("end", toDate);
        transactionInfo.addProperty("id", userId);

        return makeResponse(transactionService.getTransactionReport(transactionInfo.toString()));
    }

    @PutMapping(consumes = "application/json", path = "/transaction", produces = "application/json")
    public ResponseEntity<String> makeRefund(@RequestBody String transactionId) {
        return makeResponse(paymentService.makeRefund(transactionId));
    }

    /**
     * @author Lasse S Mørk - s154235
     */
    private ResponseEntity<String> makeResponse(String json) {
        GenericResponse response = GenericResponse.fromJson(json);
        System.out.println(json);

        if (response == null || response.getResult() == null || response.getResult().equals("null")) {
            return new ResponseEntity<>("Something went wrong, better go down the rabbit hole", INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response.getResult(), HttpStatus.valueOf(response.getCode()));
    }
}
