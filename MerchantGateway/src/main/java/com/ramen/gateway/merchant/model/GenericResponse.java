package com.ramen.gateway.merchant.model;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.util.function.Function;

public class GenericResponse {
    public static final Gson gson = new Gson();
    public final boolean success;
    public final String result;
    public final int code;

    private GenericResponse(String response, int code) {
        this.success = code >= 200 && code < 300;
        this.result = response;
        this.code = code;
    }

    public static <T> GenericResponse response(T response) {
        return response(response, 200);
    }

    public static <T> GenericResponse response(T response, int code) {
        return response(gson.toJson(response), 200);
    }

    public static GenericResponse response(String response) {
        return response(response, 200);
    }

    public static GenericResponse response(String response, int code) {
        return new GenericResponse(response, code);
    }

    public static GenericResponse failure(String response) {
        return response(response, 400);
    }

    public static GenericResponse failure(Throwable exception) {
        return failure(exception.getMessage());
    }

    public static GenericResponse failure(String response, int code) {
        return response(response, code);
    }

    public static GenericResponse failure(Throwable response, int code) {
        return failure(response.getMessage(), code);
    }

    public static GenericResponse fromJson(String json) {
        try {
            return gson.fromJson(json, GenericResponse.class);
        } catch (JsonParseException e) {
            return failure(e);
        }
    }

    public int getCode() {
        return code;
    }

    public String getResult() {
        return result;
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean isFailure() {
        return !success;
    }

    public String toJson() {
        return gson.toJson(this);
    }

    public GenericResponse handle(Function<GenericResponse, GenericResponse> func) {
        return func.apply(this);
    }

}
