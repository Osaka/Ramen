package com.ramen.gateway.merchant.services;

public interface IMerchantService {

    String createMerchant(String merchant);

    String updateMerchant(String merchant);

    String deleteMerchant(String info);

    String getMerchant(String id);
}
