package com.ramen.gateway.merchant.services;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


/**
 * Communicates with RabbitMQ broker for MerchantService
 * @author Alexander Armstrong
 */
@Service
public class MerchantService implements IMerchantService {
    private final static String QUEUE_CREATE = "merchant.create";
    private final static String QUEUE_DELETE = "merchant.delete";
    private final static String QUEUE_UPDATE = "merchant.update";
    private final static String QUEUE_GET = "merchant.get";

    private final RabbitTemplate template;

    private final DirectExchange exchange;

    @Autowired
    public MerchantService(RabbitTemplate template, @Qualifier("merchant") DirectExchange exchange) {
        this.template = template;
        this.exchange = exchange;
    }

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    @Override
    public String createMerchant(String merchant) {
        return send(QUEUE_CREATE, merchant);
    }

    @Override
    public String updateMerchant(String merchant) {
        return send(QUEUE_UPDATE, merchant);
    }

    @Override
    public String deleteMerchant(String info) {
        return send(QUEUE_DELETE, info);
    }

    @Override
    public String getMerchant(String id) {
        return send(QUEUE_GET, id);
    }
}
