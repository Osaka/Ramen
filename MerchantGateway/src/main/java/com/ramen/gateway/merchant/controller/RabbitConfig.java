package com.ramen.gateway.merchant.controller;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    @Qualifier("payment")
    public DirectExchange paymentExchange() {
        return new DirectExchange("payment");
    }

    @Bean
    @Qualifier("transaction")
    public DirectExchange transactionExchange() {
        return new DirectExchange("transaction");
    }

    @Bean
    @Qualifier("merchant")
    public DirectExchange merchantExchange() {
        return new DirectExchange("merchant");
    }
}
