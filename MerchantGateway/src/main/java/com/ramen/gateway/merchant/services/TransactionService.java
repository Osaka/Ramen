package com.ramen.gateway.merchant.services;

import com.google.gson.Gson;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author Mathias E. Asmussen - s154219
 */
@Service
public class TransactionService implements ITransactionService {
    private final static String QUEUE_TRANSACTIONREPORT = "transaction.merchantReport";

    @Autowired
    private RabbitTemplate template;

    @Autowired
    @Qualifier("transaction")
    private DirectExchange exchange;

    private Gson gson = new Gson();

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    @Override
    public String getTransactionReport(String jsonInput) {
        return send(QUEUE_TRANSACTIONREPORT, jsonInput);
    }
}
