package com.ramen.gateway.merchant.services;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PaymentService implements IPaymentService {

    private final static String QUEUE_PAY = "payment.pay";
    private final static String QUEUE_REFUNDQUEUE = "payment.refund";

    @Autowired
    private RabbitTemplate template;

    @Autowired
    @Qualifier("payment")
    private DirectExchange exchange;

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    public String makePayment(String paymentInfo) {
        return send(QUEUE_PAY, paymentInfo);
    }

    @Override
    public String makeRefund(String transactionId) {
        return send(QUEUE_REFUNDQUEUE, transactionId);
    }
}
