package com.ramen.gateway.customer.controller;

import com.ramen.gateway.customer.library.ITokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class BarcodeController {

    @Autowired
    private ITokenService tokenService;

    @GetMapping(value = "/barcode/{uuid}")
    public String barcodeGenerator(@PathVariable String uuid, Model model) {
        String barcode = tokenService.generateBarcode(uuid);

        if (barcode.equals("error")) return "error";
        model.addAttribute("image", barcode);
        return "barcode";
    }
}
