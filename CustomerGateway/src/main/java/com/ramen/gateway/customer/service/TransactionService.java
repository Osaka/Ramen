package com.ramen.gateway.customer.service;

import com.ramen.gateway.customer.library.ITransactionService;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author Mathias E. Asmussen - s154219
 */
@Service
public class TransactionService implements ITransactionService {
    // Queue names
    public final static String QUEUE_GETTRANSACTIONREPORT = "transaction.customerReport";

    @Autowired
    private RabbitTemplate template;

    @Autowired
    @Qualifier("transactionExchange")
    private DirectExchange exchange;

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    @Override
    public String getTransactionReport(String jsonInput) {
        return send(QUEUE_GETTRANSACTIONREPORT, jsonInput);
    }
}

