package com.ramen.gateway.customer.library;

import java.util.UUID;

public interface ITokenService {

    String requestTokens(String input);

    String generateBarcode(String input);

    String getTokens(UUID customerId);
}
