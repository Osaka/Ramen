package com.ramen.gateway.customer.library;

public interface ITransactionService {
    String getTransactionReport(String jsonInput);
}
