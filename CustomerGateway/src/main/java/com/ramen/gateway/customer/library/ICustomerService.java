package com.ramen.gateway.customer.library;

import java.util.UUID;

public interface ICustomerService {
    String getCustomer(UUID id);

    String createCustomer(String customer);

    String updateCustomer(String customer);

    String deleteCustomer(UUID id);
}
