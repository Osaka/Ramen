package com.ramen.gateway.customer.controller;

import com.google.gson.JsonObject;
import com.ramen.gateway.customer.model.GenericResponse;
import com.ramen.gateway.customer.library.ICustomerService;
import com.ramen.gateway.customer.library.ITokenService;
import com.ramen.gateway.customer.library.ITransactionService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

import static com.ramen.gateway.customer.model.GenericResponse.fromJson;
import static com.ramen.gateway.customer.model.GenericResponse.gson;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@CrossOrigin(origins = {"http://localhost:8888", "http://02267-osaka.compute.dtu.dk:8888"}, maxAge = 3600)
@Controller
@RestController
@RequestMapping("customer")
public class RESTController {

    private final ITransactionService transactionService;
    private final ICustomerService customerService;
    private final ITokenService tokenService;

    private class Token {
        UUID id;
        UUID ownerId;
    }

    class TokenRequest {
        UUID id;
        String url;

        public TokenRequest(UUID id) {
            this.id = id;
            this.url = "02267-osaka.compute.dtu.dk/barcode/" + id.toString();
        }
    }

    @Autowired
    public RESTController(ITransactionService transactionService, ICustomerService customerService, ITokenService tokenService) {
        this.transactionService = transactionService;
        this.customerService = customerService;
        this.tokenService = tokenService;
    }

    /**
     * Gets an existing customer
     * @author Alexander Armstrong
     * @param id
     * @return customer object json formatted
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<String> getCustomer(@PathVariable UUID id) {
        return makeResponse(customerService.getCustomer(id));
    }

    /**
     * @author Lasse Spindler Mørk (s154235)
     */
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> createCustomer(@RequestBody String customer) {
        return makeResponse(customerService.createCustomer(customer));
    }

    /**
     * @author Alexander Armstrong
     * @param id of the customer to delete
     * @return whether delete was successful
     */
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<String> deleteCustomer(@PathVariable UUID id) {
        return makeResponse(customerService.deleteCustomer(id));
    }

    /**
     * @param customer information as a json string
     * @return updated customer object as json
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @PutMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> updateCustomer(@RequestBody String customer) {
        return makeResponse(customerService.updateCustomer(customer));
    }

    /**
     * @param jsonInput containing customerId and amount of tokens requested
     * @return json containing tokenId and url to view barcode
     * @author Alexander Armstrong
     */
    @PostMapping(path = "token", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> requestTokens(@RequestBody String jsonInput) {
        // Request tokens
        String response = tokenService.requestTokens(jsonInput);

        // Parse to standard response
        GenericResponse resp = GenericResponse.fromJson(response);

        // If call is successful create new json containing urls
        if (resp.success) {
            Token[] tokens = gson.fromJson(resp.result, Token[].class);
            JsonObject json = new JsonObject();
            ArrayList<TokenRequest> responseList = new ArrayList<>();
            for (Token token : tokens) {
                responseList.add(new TokenRequest(token.id));
            }

            // Create new response to use to respond
            GenericResponse newResp = GenericResponse.response(responseList);
            return makeResponse(gson.toJson(newResp));
        }

        // If failed simply return tokenService response
        return makeResponse(response);
    }

    /**
     * @author Alexander Armstrong
     * @param customerId
     * @return tokens belonging to the customer and websites to go to to view barcodes
     */
    @GetMapping(path = "token/{customerId}", produces = "application/json")
    public ResponseEntity<String> getTokens(@PathVariable UUID customerId) {
        return makeResponse(tokenService.getTokens(customerId));
    }

    /**
     * Return a list of transactions for that user within the time span
     * @author Mathias E. Asmussen - s154219
     * @param userId
     * @param fromDate in epoch seconds
     * @param toDate in epoch seconds
     * @return
     * @throws JSONException
     */
    @GetMapping(path = "/transaction/{userId}/{fromDate}/{toDate}", produces = "application/json")
    public ResponseEntity<String> getTransaction(@PathVariable String userId, @PathVariable String fromDate,
                                                 @PathVariable String toDate) throws JSONException {
        JSONObject transactionInfo = new JSONObject();
        transactionInfo.put("start", fromDate);
        transactionInfo.put("end", toDate);
        transactionInfo.put("id", userId);

        return makeResponse(transactionService.getTransactionReport(transactionInfo.toString()));
    }

    /**
     * @author Mathias E. Asmussen - s154219
     * @param json string containing the body of the result
     * @return
     */
    private ResponseEntity<String> makeResponse(String json) {
        GenericResponse response = fromJson(json);

        if (response == null || response.getResult() == null || response.getResult().equals("null")) {
            return new ResponseEntity<>("Queue not found", INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response.getResult(), HttpStatus.valueOf(response.getCode()));
    }

}
