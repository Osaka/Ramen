package com.ramen.gateway.customer.controller;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Mathias E Asmussen (s154219)
 */
@Configuration
public class RabbitConfig {

    @Bean
    @Qualifier("customerExchange")
    public DirectExchange customerExchange() {
        return new DirectExchange("customer");
    }

    @Bean
    @Qualifier("tokenExchange")
    public DirectExchange tokenExchange() {
        return new DirectExchange("token");
    }

    @Bean
    @Qualifier("transactionExchange")
    public DirectExchange transactionExchange() {
        return new DirectExchange("transaction");
    }
}
