package com.ramen.gateway.customer.service;

import com.ramen.gateway.customer.library.ITokenService;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class TokenService implements ITokenService {
    // Queue names
    private final static String QUEUE_REQUESTTOKEN = "token.request";
    private final static String QUEUE_GENERATEBARCODE = "token.generateBarcode";
    private static final String QUEUE_GET = "token.get";

    @Autowired
    private RabbitTemplate template;

    @Autowired
    @Qualifier("tokenExchange")
    private DirectExchange exchange;

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    public String requestTokens(String input) {
        return send(QUEUE_REQUESTTOKEN, input);
    }

    public String generateBarcode(String input) {
        return send(QUEUE_GENERATEBARCODE, input);
    }

    @Override
    public String getTokens(UUID customerId) {
        return send(QUEUE_GET, customerId.toString());
    }
}
