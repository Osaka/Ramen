package com.ramen.gateway.customer.service;

import com.ramen.gateway.customer.library.ICustomerService;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CustomerService implements ICustomerService {
    // Queue names
    private static final String QUEUE_DELETECUSTOMER = "customer.delete";
    private static final String QUEUE_GETCUSTOMER = "customer.get";
    private static final String QUEUE_CREATECUSTOMER = "customer.create";
    private static final String QUEUE_UPDATECUSTOMER = "customer.update";

    @Autowired
    private RabbitTemplate template;

    @Autowired
    @Qualifier("customerExchange")
    private DirectExchange exchange;

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    public String getCustomer(UUID id) {
        return send(CustomerService.QUEUE_GETCUSTOMER, id.toString());
    }

    public String createCustomer(String customer) {
        return send(CustomerService.QUEUE_CREATECUSTOMER, customer);
    }

    @Override
    public String deleteCustomer(UUID id) {
        return send(CustomerService.QUEUE_DELETECUSTOMER, id.toString());
    }

    public String updateCustomer(String customer) {
        return send(CustomerService.QUEUE_UPDATECUSTOMER, customer);
    }
}
