package com.ramen.service.bank.builders;

import com.ramen.service.bank.soap.User;

public class UserBuilder {

    private final User dummy = new User();

    public static UserBuilder create() {
        return new UserBuilder();
    }

    public UserBuilder withFirstName(String firstName) {
        dummy.setFirstName(firstName);
        return this;
    }

    public UserBuilder withLastName(String lastName) {
        dummy.setLastName(lastName);
        return this;
    }

    public UserBuilder withCprNumber(String cpr) {
        dummy.setCprNumber(cpr);
        return this;
    }

    public User build() {
        User user = new User();
        user.setCprNumber(dummy.getCprNumber());
        user.setLastName(dummy.getLastName());
        user.setFirstName(dummy.getFirstName());
        return user;
    }
}
