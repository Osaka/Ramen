package com.ramen.service.bank;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.ramen.service.bank.soap.BankService;
import com.ramen.service.bank.soap.BankServiceService;

@Configuration
public class AppConfig {

    @Bean
    public BankService bankService() {
        return bankServiceService().getBankServicePort();
    }

    @Bean
    BankServiceService bankServiceService() {
        return new BankServiceService();
    }
}
