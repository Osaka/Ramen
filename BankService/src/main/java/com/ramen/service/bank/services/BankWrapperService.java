package com.ramen.service.bank.services;

import com.google.gson.Gson;
import com.ramen.service.bank.model.GenericResponse;
import com.ramen.service.bank.soap.AccountInfo;
import com.ramen.service.bank.soap.BankService;
import com.ramen.service.bank.soap.BankServiceException_Exception;
import com.ramen.service.bank.soap.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

import static com.ramen.service.bank.model.GenericResponse.*;

/**
 * @author Lasse Spindler Mørk (s154235)
 */
@Service
public class BankWrapperService implements IBankWrapperService {

    private static final Gson gson = new Gson();
    private BankService bankService;

    @Autowired
    public BankWrapperService(BankService bankService) {
        this.bankService = bankService;
    }

    @Override
    public GenericResponse createAccount(User user, BigDecimal startAmount) {
        try {
            return response(bankService.createAccountWithBalance(user, startAmount));
        } catch (BankServiceException_Exception e) {
            return failure(e);
        }
    }

    @Override
    public GenericResponse deleteAccount(String accountId) {
        try {
            bankService.retireAccount(accountId);
            return response(String.format("%s successfully retired", accountId));
        } catch (BankServiceException_Exception e) {
            return failure(e);
        }
    }

    @Override
    public GenericResponse transferMoney(BigDecimal amount, String from, String to, String description) {
        try {
            bankService.transferMoneyFromTo(from, to, amount, description);
            return response(String.format("%s sent %s to %s, reason: %s", from, amount, to, description));
        } catch (BankServiceException_Exception e) {
            return failure(e);
        }
    }

    @Override
    public GenericResponse getAccount(String accountId) {
        try {
            return response(gson.toJson(bankService.getAccount(accountId)));
        } catch (BankServiceException_Exception e) {
            return failure(e);
        }
    }

    @Override
    public GenericResponse getAccountByCpr(String cprNumber) {
        try {
            return response(gson.toJson(bankService.getAccountByCprNumber(cprNumber)));
        } catch (BankServiceException_Exception e) {
            return failure(e);
        }
    }

    @Override
    public List<AccountInfo> getAccounts() {
        return bankService.getAccounts();
    }
}
