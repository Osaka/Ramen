package com.ramen.service.bank.controller;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Lasse Spindler Mørk (s154235)
 */
@Configuration
public class RabbitConfig {

    public static final String
            QUEUE_BANK_TRANSFER = "bank.transfer",
            QUEUE_BANK_CREATE = "bank.create",
            QUEUE_BANK_ACCOUNT = "bank.account",
            EXCHANGE = "bank";

    public static class ServerConfig {
        @Bean
        public Queue queueTransfer() {
            return new Queue(QUEUE_BANK_TRANSFER, true);
        }

        @Bean
        public Queue queueCreate() {
            return new Queue(QUEUE_BANK_CREATE, true);
        }

        @Bean
        public Queue queueAccount() {
            return new Queue(QUEUE_BANK_ACCOUNT, true);
        }

        @Bean
        public DirectExchange exchange() {
            return new DirectExchange(EXCHANGE);
        }

        @Bean
        public Binding bindingTransfer(DirectExchange exchange, Queue queueTransfer) {
            return BindingBuilder.bind(queueTransfer).to(exchange).with(queueTransfer.getName());
        }

        @Bean
        public Binding bindingCreate(DirectExchange exchange, Queue queueCreate) {
            return BindingBuilder.bind(queueCreate).to(exchange).with(queueCreate.getName());
        }

        @Bean
        public Binding bindingAccount(DirectExchange exchange, Queue queueAccount) {
            return BindingBuilder.bind(queueAccount).to(exchange).with(queueAccount.getName());
        }

        @Bean
        public RabbitServer server() {
            return new RabbitServer();
        }
    }
}
