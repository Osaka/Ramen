package com.ramen.service.bank.controller;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.ramen.service.bank.model.GenericResponse;
import com.ramen.service.bank.model.GetAccountInput;
import com.ramen.service.bank.model.TransferInput;
import com.ramen.service.bank.services.IBankWrapperService;
import com.ramen.service.bank.soap.User;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.math.BigDecimal;

import static com.ramen.service.bank.model.GenericResponse.failure;

/**
 * @author Lasse Spindler Mørk (s154235)
 */
@Controller
public class RabbitServer {

    private static final Gson gson = new Gson();

    @Autowired
    private IBankWrapperService bank;

    @RabbitListener(queues = RabbitConfig.QUEUE_BANK_CREATE)
    public String createAccount(String json) {
        User user;
        try {
            user = gson.fromJson(json, User.class);
        } catch (Exception e) {
            return failure(e).toJson();
        }

        GenericResponse response = bank.createAccount(user, BigDecimal.valueOf(1000));

        return response.toJson();
    }

    @RabbitListener(queues = RabbitConfig.QUEUE_BANK_ACCOUNT)
    public String getAccount(String json) {
        GetAccountInput input;
        try {
            input = gson.fromJson(json, GetAccountInput.class);
        } catch (Exception e) {
            return failure(e, 500).toJson();
        }
        GenericResponse response = bank.getAccount(input.getAccountId());
        return response.toJson();
    }

    @RabbitListener(queues = RabbitConfig.QUEUE_BANK_TRANSFER)
    public String transferMoney(String json) {
        TransferInput input;
        try {
            input = gson.fromJson(json, TransferInput.class);
        } catch (Exception e) {
            return failure(e, 500).toJson();
        }

        GenericResponse response = bank.transferMoney(
                input.getAmount(),
                input.getFromAccount(),
                input.getToAccount(),
                input.getDescription());
        return response.toJson();
    }
}
