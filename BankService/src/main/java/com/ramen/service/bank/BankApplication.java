package com.ramen.service.bank;

import com.ramen.service.bank.services.IBankWrapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class BankApplication implements CommandLineRunner {

    private IBankWrapperService bank;

    @Autowired
    public BankApplication(IBankWrapperService bankWrapperService) {
        this.bank = bankWrapperService;
    }

    public static void main(String[] args) {
        SpringApplication.run(BankApplication.class, args);
    }

    @Override
    public void run(String... args) {
    }
}

