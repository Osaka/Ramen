package com.ramen.service.bank.model;

import java.math.BigDecimal;

public class TransferInput {
    private String description, fromAccount, toAccount;
    private BigDecimal amount;

    public String getDescription() {
        return description;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public String getToAccount() {
        return toAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
