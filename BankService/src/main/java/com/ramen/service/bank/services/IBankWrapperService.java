package com.ramen.service.bank.services;

import com.ramen.service.bank.model.GenericResponse;
import com.ramen.service.bank.soap.AccountInfo;
import com.ramen.service.bank.soap.User;

import java.math.BigDecimal;
import java.util.List;

public interface IBankWrapperService {
    /**
     * @param user
     * @param startAmount
     * @return response with success status and UUID if successful otherwise of reason
     */
    GenericResponse createAccount(User user, BigDecimal startAmount);

    /**
     * @param accountId
     * @return response with success status and error reason if failed
     */
    GenericResponse deleteAccount(String accountId);

    /**
     * @param amount
     * @param description
     * @param from
     * @param to
     * @return response with success status and error reason if failed
     */
    GenericResponse transferMoney(BigDecimal amount, String from, String to, String description);

    /**
     * @param accountId
     * @return responses with null if failure otherwise returns account object
     */
    GenericResponse getAccount(String accountId);

    /**
     * @param cprNumber
     * @return responses with null if failure otherwise returns account object
     */
    GenericResponse getAccountByCpr(String cprNumber);

    /**
     * @return list of accounts available
     */
    List<AccountInfo> getAccounts();

}
