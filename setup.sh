echo Creating .env files...

if [ ! -f ./BankService/.env ]; then
    touch ./BankService/.env
fi

if [ ! -f ./PaymentService/.env ]; then
    touch ./PaymentService/.env
fi

if [ ! -f ./TransactionService/.env ]; then
    touch ./TransactionService/.env
fi

if [ ! -f ./TokenService/.env ]; then
    touch ./TokenService/.env
fi

if [ ! -f ./CustomerService/.env ]; then
    touch ./CustomerService/.env
fi

if [ ! -f ./MerchantService/.env ]; then
    touch ./MerchantService/.env
fi

if [ ! -f ./CustomerGateway/.env ]; then
    touch ./CustomerGateway/.env
fi

if [ ! -f ./MerchantGateway/.env ]; then
    touch ./MerchantGateway/.env
fi

echo All .env files present...