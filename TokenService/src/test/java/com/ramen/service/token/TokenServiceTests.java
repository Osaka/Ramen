package com.ramen.service.token;

import com.ramen.service.token.repository.ITokenRepository;
import com.ramen.service.token.models.Token;
import com.ramen.service.token.services.ITokenService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Alexander Armstrong
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TokenServiceTests {

    @Autowired
    private ITokenRepository tokenRepository;

    @Autowired
    private ITokenService tokenService;

    @Test
    public void consumeTokenTest() {
        // Setup
        Token token = new Token(UUID.randomUUID());
        tokenRepository.saveAndFlush(token);
        assertThat(
                tokenRepository.findById(token.getId()).orElse(null),
                is(not(equalTo(null))));

        // Act
        tokenService.consumeToken(token.getId());

        // Assert
        assertThat(
                tokenRepository.findById(token.getId()).orElse(null),
                is(equalTo(null)));
    }

    @Test
    public void consumeNonExistentToken() {
        assertThat(tokenService.consumeToken(UUID.randomUUID()), is(equalTo(null)));
    }

    @Test
    public void requestTokenFromZero() {
        // Setup
        UUID user = UUID.randomUUID();

        // Act
        tokenService.requestTokens(user, 1);

        // Assert
        assertThat(tokenRepository.findAllByOwnerId(user).size(), is(equalTo(1)));
    }

    @Test
    public void requestNegativeAmountOfTokens() {
        // Setup
        UUID user = UUID.randomUUID();

        // Act
        tokenService.requestTokens(user, -1);

        // Assert
        assertThat(tokenRepository.findAllByOwnerId(user).size(), is(equalTo(0)));
    }

    @Test
    public void requestTooManyTokens() {
        // Setup
        UUID user = UUID.randomUUID();

        // Act
        tokenService.requestTokens(user, 7);

        // Assert
        assertThat(tokenRepository.findAllByOwnerId(user).size(), is(equalTo(0)));
    }

    @Test
    public void requestAdditionalTokensAboveOne() {
        // Setup
        UUID user = UUID.randomUUID();
        tokenService.requestTokens(user, 3);

        // Act
        tokenService.requestTokens(user, 1);

        // Assert
        assertThat(tokenRepository.findAllByOwnerId(user).size(), is(equalTo(3)));
    }

    @Test
    public void requestAdditionalTokensAtOne() {
        // Setup
        UUID user = UUID.randomUUID();
        tokenService.requestTokens(user, 1);

        // Act
        tokenService.requestTokens(user, 3);

        // Assert
        assertThat(tokenRepository.findAllByOwnerId(user).size(), is(equalTo(4)));
    }
}
