package com.ramen.service.token;

import com.google.zxing.WriterException;
import com.ramen.service.token.services.QrCodeGenerator;
import com.ramen.service.token.services.IBarcodeGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TokenServiceApplicationTests {

    /**
     * @author Alexander Armstrong
     * Simple test to ensure the generator api works the way it is expected to.
     */
    @Test
    public void encodeBarcodeTest() throws IOException, WriterException {
        // Simple test to ensure the QR works the way we expect it to.
        IBarcodeGenerator bG = new QrCodeGenerator(100, 100);
        String encodedQrCode1 = bG.generateBase64Barcode("Alexander");
        String encodedQrCode2 = bG.generateBase64Barcode("Alexander");
        assertEquals(encodedQrCode1, encodedQrCode2);
    }
}

