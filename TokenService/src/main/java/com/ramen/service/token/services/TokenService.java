package com.ramen.service.token.services;

import com.ramen.service.token.repository.ITokenRepository;
import com.ramen.service.token.models.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service for handling tokens
 *
 * @author Tobias Lindstrøm (s15335)
 */
@Service
public class TokenService implements ITokenService {

    private final ITokenRepository tokenRepository;

    @Autowired
    public TokenService(ITokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public List<Token> requestTokens(UUID customerId, int amountOfTokens) {

        List<Token> tokens = new ArrayList<>();

        if (amountOfTokens < 1 || amountOfTokens > 6) {
            return tokens;
        }

        int currentAmountOfTokens = tokenRepository.findAllByOwnerId(customerId).size();
        if (currentAmountOfTokens <= 1 && currentAmountOfTokens + amountOfTokens <= 6) {
            for (int i = 0; i < amountOfTokens; i++) {
                Token token = tokenRepository.save(new Token(customerId));
                tokens.add(token);
            }
            tokenRepository.flush();
        }
        return tokens;
    }

    @Override
    public UUID consumeToken(UUID tokenId) {
        if (tokenId == null) return null;

        Optional<Token> token = tokenRepository.findById(tokenId);

        if (token.isPresent()) {
            UUID id = token.get().getOwnerId();
            tokenRepository.delete(token.get());
            return id;
        }
        return null;
    }

    @Override
    public List<Token> getTokensForCustomer(UUID customerId) {
        return tokenRepository.findAllByOwnerId(customerId);
    }

}
