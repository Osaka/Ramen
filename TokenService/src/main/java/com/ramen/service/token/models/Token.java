package com.ramen.service.token.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.UUID;

/**
 * @author Tobias Lindstrøm (s15335)
 */
@Entity
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty("id")
    private UUID id;

    @JsonProperty("ownerId")
    private UUID ownerId;

    public Token() {
    }

    public Token(UUID ownerId) {
        this.ownerId = ownerId;
    }

    @JsonCreator
    public Token(@JsonProperty("id") UUID id, @JsonProperty("ownerId") UUID ownerId) {
        this.id = id;
        this.ownerId = ownerId;
    }

    public static Token fromUUIDString(String uuid) {
        return new Token(UUID.fromString(uuid), null);
    }

    public UUID getId() {
        return id;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public String toString() {
        return id.toString();
    }
}
