package com.ramen.service.token.controller;

import com.google.gson.JsonParseException;
import com.google.zxing.WriterException;
import com.ramen.service.token.models.RequestTokensInput;
import com.ramen.service.token.models.Token;
import com.ramen.service.token.services.IBarcodeGenerator;
import com.ramen.service.token.services.ITokenService;
import com.ramen.service.token.services.QrCodeGenerator;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static com.ramen.service.token.models.GenericResponse.*;

public class RabbitServer {

    @Autowired
    private ITokenService tokenService;

    @RabbitListener(queues = RabbitConfig.QUEUE_TOKEN_REQUEST)
    public String requestTokens(String jsonString) {
        RequestTokensInput input;

        try {
            input = gson.fromJson(jsonString, RequestTokensInput.class);
            if (input.customerId == null)
                return failure("Invalid customer id").toJson();
        } catch (JsonParseException e) {
            return failure(e).toJson();
        }

        List<Token> tokens = tokenService.requestTokens(input.customerId, input.amount);

        if (tokens.isEmpty()) {
            return failure("Invalid arguments").toJson();
        }

        return response(tokens).toJson();
    }

    /**
     * @author Mathias E. Asmussen - s154219
     * @param jTokenId
     * @return
     */
    @RabbitListener(queues = RabbitConfig.QUEUE_TOKEN_CONSUME)
    public String consumeToken(String jTokenId) {
        UUID tokenId;
        try {
            tokenId = UUID.fromString(jTokenId);
        } catch (IllegalArgumentException e) {
            return failure(e).toJson();
        }

        UUID userId = tokenService.consumeToken(tokenId);

        if (userId == null) {
            return failure("Could not consume token").toJson();
        } else {
            return response(userId.toString()).toJson();
        }
    }

    @RabbitListener(queues = RabbitConfig.QUEUE_TOKEN_GENERATEBARCODE)
    public String generateBarcode(String uuid) {
        IBarcodeGenerator generator = new QrCodeGenerator(200, 200);

        try {
            return generator.generateBase64Barcode(uuid);
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }
        return "error";
    }

    @RabbitListener(queues = RabbitConfig.QUEUE_TOKEN_GET)
    public String getTokens(String customerId) {
        try {
            return response(tokenService.getTokensForCustomer(UUID.fromString(customerId))).toJson();
        } catch (IllegalArgumentException e) {
            return failure(e).toJson();
        }
    }
}
