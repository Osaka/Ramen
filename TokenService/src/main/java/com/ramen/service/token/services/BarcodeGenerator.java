package com.ramen.service.token.services;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

public abstract class BarcodeGenerator implements IBarcodeGenerator {

    private int width, height;
    private BarcodeFormat format;

    /**
     * @param width  of the barcode
     * @param height of the barcode
     * @param format of the barcode
     * @author Alexander Armstrong
     * Constructor to set size and format of barcode
     */
    BarcodeGenerator(int width, int height, BarcodeFormat format) {
        this.width = width;
        this.height = height;
        this.format = format;
    }

    /**
     * @author Alexander Armstrong
     * {@inheritDoc}
     */
    @Override
    public byte[] generateByteBarcode(String text) throws WriterException, IOException {
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = writer.encode(text, format, width, height);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "png", outputStream);

        return outputStream.toByteArray();
    }

    /**
     * @author Mathias E. Asmussen - s154219
     * {@inheritDoc}
     */
    @Override
    public String generateBase64Barcode(String text) throws WriterException, IOException {
        byte[] byteArr = generateByteBarcode(text);
        return Base64.getEncoder().encodeToString(byteArr);
    }
}
