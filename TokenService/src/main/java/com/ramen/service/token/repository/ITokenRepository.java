package com.ramen.service.token.repository;

import com.ramen.service.token.models.Token;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ITokenRepository extends JpaRepository<Token, UUID> {

    List<Token> findAllByOwnerId(UUID ownerId);

}
