package com.ramen.service.token;

import io.github.cdimascio.dotenv.Dotenv;

/**
 * Settings used to retrieve environment variables
 */
public class Settings {

    public static Dotenv env;

    /**
     * @param setting the environment variable to retrieve
     * @return the value of the variable
     * @author Alexander Armstrong
     * Get a specific environment variable.
     * System variables take precedent over variables defined in the .env file.
     */
    public static String get(String setting) {
        String valueFromEnv = System.getenv(setting);
        if (valueFromEnv != null) {
            return valueFromEnv;
        }
        env = Dotenv.load();
        return env.get(setting);
    }
}