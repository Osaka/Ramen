package com.ramen.service.token.services;

import com.google.zxing.WriterException;

import java.io.IOException;

/**
 * Used to generate Bar codes.
 */
public interface IBarcodeGenerator {

    /**
     * Generates a token as a byte array.
     *
     * @param text you would like to have encoded
     * @return byte array containing the token
     * @throws WriterException
     * @throws IOException
     */
    byte[] generateByteBarcode(String text) throws WriterException, IOException;

    /**
     * Generates a token as a Base64 encoded String
     *
     * @param text you would like to have encoded
     * @return Base64 encoded string containing the token
     * @throws WriterException
     * @throws IOException
     */
    String generateBase64Barcode(String text) throws WriterException, IOException;
}
