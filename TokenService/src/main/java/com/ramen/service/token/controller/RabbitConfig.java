package com.ramen.service.token.controller;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Alexander Armstrong
 */
@Configuration
public class RabbitConfig {

    public final static String
            QUEUE_TOKEN_REQUEST = "token.request",
            QUEUE_TOKEN_CONSUME = "token.consume",
            QUEUE_TOKEN_GENERATEBARCODE = "token.generateBarcode",
            QUEUE_TOKEN_GET = "token.get";

    public static class ServerConfig {
        @Bean
        public Queue queueRequestToken() {
            return new Queue(QUEUE_TOKEN_REQUEST, true);
        }

        @Bean
        public Queue queueConsumeToken() {
            return new Queue(QUEUE_TOKEN_CONSUME, true);
        }

        @Bean
        public Queue queueGenerateBarcode() {
            return new Queue(QUEUE_TOKEN_GENERATEBARCODE, true);
        }

        @Bean
        public Queue queueGet() {
            return new Queue(QUEUE_TOKEN_GET, true);
        }

        @Bean
        public DirectExchange tokenExchange() {
            return new DirectExchange("token");
        }

        @Bean
        public Binding bindingRequestToken(DirectExchange tokenExchange, Queue queueRequestToken) {
            return BindingBuilder.bind(queueRequestToken).to(tokenExchange).with(queueRequestToken.getName());
        }

        @Bean
        public Binding bindingConsumeToken(DirectExchange tokenExchange, Queue queueConsumeToken) {
            return BindingBuilder.bind(queueConsumeToken).to(tokenExchange).with(queueConsumeToken.getName());
        }

        @Bean
        public Binding bindingGenerateBarcode(DirectExchange tokenExchange, Queue queueGenerateBarcode) {
            return BindingBuilder.bind(queueGenerateBarcode).to(tokenExchange).with(queueGenerateBarcode.getName());
        }

        @Bean
        public Binding bindingGet(DirectExchange tokenExchange, Queue queueGet) {
            return BindingBuilder.bind(queueGet).to(tokenExchange).with(queueGet.getName());
        }

        @Bean
        public RabbitServer server() {
            return new RabbitServer();
        }
    }
}
