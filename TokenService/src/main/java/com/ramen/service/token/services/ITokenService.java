package com.ramen.service.token.services;

import com.ramen.service.token.models.Token;

import java.util.List;
import java.util.UUID;

public interface ITokenService {

    List<Token> requestTokens(UUID customerId, int amountOfTokens);

    UUID consumeToken(UUID tokenId);

    List<Token> getTokensForCustomer(UUID customerId);

}
