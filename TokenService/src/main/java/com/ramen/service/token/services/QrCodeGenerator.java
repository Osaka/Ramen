package com.ramen.service.token.services;

import com.google.zxing.BarcodeFormat;

public class QrCodeGenerator extends BarcodeGenerator {

    public QrCodeGenerator(int width, int height) {
        super(width, height, BarcodeFormat.QR_CODE);
    }
}