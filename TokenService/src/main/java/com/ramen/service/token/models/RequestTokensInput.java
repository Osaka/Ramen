package com.ramen.service.token.models;


import java.util.UUID;

public class RequestTokensInput {
    public UUID customerId;
    public int amount;
}