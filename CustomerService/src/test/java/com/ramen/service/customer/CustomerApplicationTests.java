package com.ramen.service.customer;

import com.ramen.service.customer.model.Customer;
import com.ramen.service.customer.repository.ICustomerRepository;
import com.ramen.service.customer.services.ICustomerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerApplicationTests {

    @Autowired
    private ICustomerRepository customerRepository;

    @Autowired
    private ICustomerService service;

    /**
     * @author Alexander Armstrong
     */
    @Test
    public void createCustomerTest() {
        Customer customer = service.createCustomer("John", "John", UUID.randomUUID());
        customerRepository.saveAndFlush(customer);

        assertThat(customer.getId(), is(notNullValue()));
        assertThat(customerRepository.findAll().size(), is(1));
    }

    /**
     * @author Alexander Armstrong
     */
    @Test
    public void deleteExistingCustomer() {
        // Setup
        Customer customerToBeDeleted = service.createCustomer("John", "John", UUID.randomUUID());
        customerRepository.saveAndFlush(customerToBeDeleted);

        // Act
        service.deleteCustomer(customerToBeDeleted.getId());

        // Assert
        assertThat(customerRepository.findById(customerToBeDeleted.getId()).orElse(null), is(equalTo(null)));
    }

    /**
     * @author Alexander Armstrong
     */
    @Test
    public void deleteNonExistingCustomer() {
        assertThat(service.deleteCustomer(UUID.randomUUID()), is(equalTo(false)));
    }

    /**
     * @author Alexander Armstrong
     */
    @Test
    public void getExistingCustomer() {
        // Setup
        Customer customer = service.createCustomer("John", "John", UUID.randomUUID());
        customerRepository.saveAndFlush(customer);

        // Act
        Customer getCustomer = service.getCustomer(customer.getId());

        // Assert
        assertThat(getCustomer, is(equalTo(customer)));
    }

    /**
     * @author Alexander Armstrong
     */
    @Test
    public void getNonExistingCustomer() {
        // Act
        Customer getCustomer = service.getCustomer(UUID.randomUUID());

        // Assert
        assertThat(getCustomer, is(equalTo(null)));
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @Test
    public void updateNonExistingCustomer() {
        // Setup
        Customer customer = service.createCustomer("a", "b", UUID.randomUUID());
        Boolean customerDeleted = service.deleteCustomer(customer.getId());

        // Act
        Customer updatedCustomer = service.updateCustomer(customer);

        // Assert
        assertTrue(customerDeleted);
        assertNull(updatedCustomer);
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @Test
    public void updateExistingCustomer() {
        // Setup
        Customer customer = service.createCustomer("a", "b", UUID.randomUUID());

        String newFirstName = "c";
        String newLastName = "d";
        UUID newAccountNumber = UUID.randomUUID();

        // Act
        customer.setFirstName(newFirstName);
        customer.setLastName(newLastName);
        customer.setAccountNumber(newAccountNumber);

        Customer updatedCustomer = service.updateCustomer(customer);

        // Assert
        assertNotNull(updatedCustomer);
        assertEquals(customer.getId(), updatedCustomer.getId());
        assertEquals(newFirstName, updatedCustomer.getFirstName());
        assertEquals(newLastName, updatedCustomer.getLastName());
        assertEquals(newAccountNumber, updatedCustomer.getAccountNumber());
    }

}

