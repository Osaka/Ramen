package com.ramen.service.customer.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;

/**
 * @author Tobias Lindstrøm (s15335)
 *
 * Customer model class
 */

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String firstName, lastName;
    private UUID accountNumber;

    public Customer() { }

    public Customer(String firstName, String lastName, UUID accountId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountNumber = accountId;
    }

    public UUID getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public UUID getAccountNumber() {
        return accountNumber;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAccountNumber(UUID accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public boolean equals(Object other) {
        if(other instanceof Customer) {
            return this.id.equals(((Customer) other).getId());
        }
        return false;
    }
}
