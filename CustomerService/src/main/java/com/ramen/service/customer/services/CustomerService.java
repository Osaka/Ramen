package com.ramen.service.customer.services;

import com.ramen.service.customer.model.Customer;
import com.ramen.service.customer.repository.ICustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerService implements ICustomerService {

    private final ICustomerRepository repository;

    @Autowired
    public CustomerService(ICustomerRepository customerRepository) {
        this.repository = customerRepository;
    }


    /**
     * @author Alexander Armstrong
     * @param firstName of the customer
     * @param lastName of the customer
     * @param accountId for the customer
     * @return the created Customer object.
     */
    @Override
    public Customer createCustomer(String firstName, String lastName, UUID accountId) {
        Customer customer = new Customer(firstName, lastName, accountId);
        return repository.saveAndFlush(customer);
    }

    /**
     * @param customer object with updated values
     * @return updated customer object
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @Override
    public Customer updateCustomer(Customer customer){
        if (getCustomer(customer.getId()) == null) {
            return null;
        }

        return repository.saveAndFlush(customer);
    }

    /**
     * @author Morten Telling
     * @param id of the customer to be deleted
     * @return boolean describing whether or not the deletion succeeded.
     */
    @Override
    public boolean deleteCustomer(UUID id) {
        if (id == null) return false;

        Optional<Customer> customerToBeDeleted = repository.findById(id);
        if(customerToBeDeleted.isPresent()) {
            repository.delete(customerToBeDeleted.get());
            repository.flush();
            return true;
        }
        return false;
    }

    /**
     * @author Alexander Armstrong
     * @param id of the customer
     * @return the Customer if it exists otherwise null
     */
    @Override
    public Customer getCustomer(UUID id) {
        if (id == null) return null;
        return repository.findById(id).orElse(null);
    }
}
