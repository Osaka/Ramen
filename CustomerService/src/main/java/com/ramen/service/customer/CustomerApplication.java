package com.ramen.service.customer;

import com.ramen.service.customer.repository.ICustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerApplication {

    @Autowired
    ICustomerRepository customerRepository;

    public static void main(String[] args) {
        SpringApplication.run(CustomerApplication.class, args);
    }
}

