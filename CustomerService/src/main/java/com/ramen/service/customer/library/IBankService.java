package com.ramen.service.customer.library;

public interface IBankService {

    String createAccount(String firstName, String lastName, String cprNumber);

}
