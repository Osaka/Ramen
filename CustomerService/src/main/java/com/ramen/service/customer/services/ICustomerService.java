package com.ramen.service.customer.services;

import com.ramen.service.customer.model.Customer;

import java.util.UUID;

public interface ICustomerService {

    Customer createCustomer(String firstName, String lastName, UUID accountId);

    Customer updateCustomer(Customer customer);

    boolean deleteCustomer(UUID id);

    Customer getCustomer(UUID id);
}
