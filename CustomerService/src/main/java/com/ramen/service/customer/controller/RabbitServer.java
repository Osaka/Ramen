package com.ramen.service.customer.controller;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.ramen.service.customer.model.Customer;
import com.ramen.service.customer.services.ICustomerService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static com.ramen.service.customer.model.GenericResponse.failure;
import static com.ramen.service.customer.model.GenericResponse.response;
import static com.ramen.service.customer.controller.RabbitConfig.*;

public class RabbitServer {

    private static final Gson gson = new Gson();
    @Autowired
    private ICustomerService service;

    @RabbitListener(queues = RabbitConfig.QUEUE_CUSTOMER_GETACCOUNT)
    public String receiveGetCustomerAccount(String input) {
        UUID userId;
        try {
            userId = UUID.fromString(input);
        } catch (IllegalArgumentException e) {
            return failure(e).toJson();
        }

        Customer customer = service.getCustomer(userId);

        if (customer == null || customer.getAccountNumber() == null) {
            return failure("Invalid customer id").toJson();
        }

        return response(customer.getAccountNumber().toString()).toJson();
    }

    /**
     * @param customer to be created
     * @return the customer that has been created or an error
     * @author Alexander Armstrong
     */
    @RabbitListener(queues = RabbitConfig.QUEUE_CUSTOMER_CREATE)
    public String receiveCreateCustomer(String customer) {
        Customer parsedCustomer;

        try {
            parsedCustomer = gson.fromJson(customer, Customer.class);
        } catch (JsonParseException | IllegalArgumentException e) {
            return failure(e).toJson();
        }

        return response(service.createCustomer(
                parsedCustomer.getFirstName(),
                parsedCustomer.getLastName(),
                parsedCustomer.getAccountNumber()))
                .toJson();

    }

    /**
     * @param customer object as json
     * @return the updated customer information or an error
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @RabbitListener(queues = RabbitConfig.QUEUE_CUSTOMER_UPDATE)
    public String receiveUpdateCustomer(String customer) {
        // Parse received customer
        Customer updatedCustomer;
        try {
            updatedCustomer = gson.fromJson(customer, Customer.class);
        } catch (JsonParseException | IllegalArgumentException e) {
            return failure(e).toJson();
        }

        // Check if customer exists
        Customer existingCustomer = service.getCustomer(updatedCustomer.getId());
        if (existingCustomer == null) {
            return failure("Customer does not exist").toJson();
        }

        // Update existing customer
        if (updatedCustomer.getFirstName() != null) {
            existingCustomer.setFirstName(updatedCustomer.getFirstName());
        }
        if (updatedCustomer.getLastName() != null) {
            existingCustomer.setLastName(updatedCustomer.getLastName());
        }
        if (updatedCustomer.getAccountNumber() != null) {
            existingCustomer.setAccountNumber(updatedCustomer.getAccountNumber());
        }

        return response(service.updateCustomer(existingCustomer)).toJson();
    }

    @RabbitListener(queues = RabbitConfig.QUEUE_CUSTOMER_DELETE)
    public String receiveDeleteCustomer(String id) {
        UUID userId;
        try {
            userId = UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            return failure(e).toJson();
        }

        boolean result = service.deleteCustomer(userId);

        if (!result) return failure("Customer does not exist").toJson();

        return response(result).toJson();
    }

    @RabbitListener(queues = RabbitConfig.QUEUE_CUSTOMER_GET)
    public String receiveGetCustomer(String id) {
        UUID userId;
        try {
            userId = UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            return failure(e).toJson();
        }

        Customer customer = service.getCustomer(userId);

        return customer != null ?
                response(customer).toJson() :
                failure("Customer does not exist").toJson();
    }
}
