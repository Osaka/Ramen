package com.ramen.service.transaction.time;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public interface ITimestamp {

    void set(OffsetDateTime time);

    OffsetDateTime get();

    default void reset() {
        set(OffsetDateTime.now(ZoneOffset.UTC));
    }

}

