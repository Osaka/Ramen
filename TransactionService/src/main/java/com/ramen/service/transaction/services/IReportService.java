package com.ramen.service.transaction.services;

import com.ramen.service.transaction.model.Transaction;
import com.ramen.service.transaction.time.Timestamp;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

public interface IReportService {

    List<Transaction> createCustomerReport(OffsetDateTime from, OffsetDateTime to, UUID id);

    default List<Transaction> createCustomerReport(OffsetDateTime from, UUID id) {
        return createCustomerReport(from, Timestamp.now(), id);
    }

    List<Transaction> createMerchantReport(OffsetDateTime from, OffsetDateTime to, UUID id);

    default List<Transaction> createMerchantReport(OffsetDateTime from, UUID id) {
        return createMerchantReport(from, Timestamp.now(), id);
    }

}
