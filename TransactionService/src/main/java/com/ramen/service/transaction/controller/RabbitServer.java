package com.ramen.service.transaction.controller;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.ramen.service.transaction.library.ReportInput;
import com.ramen.service.transaction.model.Transaction;
import com.ramen.service.transaction.services.IReportService;
import com.ramen.service.transaction.services.ITransactionService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.UUID;

import static com.ramen.service.transaction.model.GenericResponse.failure;
import static com.ramen.service.transaction.model.GenericResponse.response;

@Controller
public class RabbitServer {

    private static final Gson gson = new Gson();

    @Autowired
    private IReportService reportService;

    @Autowired
    private ITransactionService transactionService;

    @RabbitListener(queues = RabbitConfig.CUSTOMER_REPORT_QUEUE)
    public String getCustomerReport(String json) {
        ReportInput input;
        try {
            input = gson.fromJson(json, ReportInput.class);
        } catch (JsonParseException e) {
            return failure(e).toJson();
        }

        List<Transaction> list = reportService.createCustomerReport(
                input.getStartTimestamp(),
                input.getEndTimestamp(),
                UUID.fromString(input.getId())
        );

        if (list == null)
            return failure("no response").toJson();

        return response(list).toJson();
    }

    @RabbitListener(queues = RabbitConfig.MERCHANT_REPORT_QUEUE)
    public String getMerchantReport(String json) {
        ReportInput input;
        try {
            input = gson.fromJson(json, ReportInput.class);

        } catch (JsonParseException e) {
            return failure(e).toJson();
        }
        List<Transaction> list = reportService.createMerchantReport(
                input.getStartTimestamp(),
                input.getEndTimestamp(),
                UUID.fromString(input.getId())
        );

        if (list == null)
            return failure("no response").toJson();
        return response(list).toJson();
    }

    /**
     * log transaction
     *
     * @param json json object of type transaction
     * @return id of the logged transaction
     * @author Tobias Lindstrøm (s15335)
     */
    @RabbitListener(queues = RabbitConfig.LOG_TRANSACTION_QUEUE)
    public String logTransaction(String json) {
        Transaction parsedTransaction = gson.fromJson(json, Transaction.class);
        Transaction transaction = transactionService.createTransaction(
                parsedTransaction.getCustomerId(),
                parsedTransaction.getMerchantId(),
                parsedTransaction.getTokenId(),
                parsedTransaction.getAmount(),
                parsedTransaction.getTimestamp(),
                parsedTransaction.getRefunded()
        );

        return response(transaction.getId().toString()).toJson();
    }

    /**
     * refund transaction
     *
     * @param json json object transaction id
     * @return the refunded transaction if success
     * @author Lasse Spindler Mørk (s154235)
     */
    @RabbitListener(queues = RabbitConfig.REFUND_TRANSACTION_QUEUE)
    public String refundTransaction(String json) {
        UUID id;
        try {
            id = gson.fromJson(json, UUID.class);
        } catch (JsonParseException e) {
            return failure(e).toJson();
        }

        if (transactionService.refundTransaction(id)) {
            return response(transactionService.getTransaction(id)).toJson();
        } else {
            return failure("Invalid transaction id, or the transaction has already been refunded").toJson();
        }
    }
}
