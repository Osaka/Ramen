package com.ramen.service.transaction.time;

import java.time.OffsetDateTime;

public class Timestamp implements ITimestamp {

    private OffsetDateTime time;

    public static OffsetDateTime now() {
        return new Timestamp().get();
    }

    public Timestamp() {
        reset();
    }

    public Timestamp(OffsetDateTime time) {
        set(time);
    }

    @Override
    public void set(OffsetDateTime time) {
        this.time = time;
    }

    @Override
    public OffsetDateTime get() {
        return time;
    }
}
