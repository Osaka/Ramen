package com.ramen.service.transaction.library;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public class ReportInput {
    private long start, end;
    private String id;

    public OffsetDateTime getStartTimestamp() {
        return OffsetDateTime.ofInstant(Instant.ofEpochSecond(start), ZoneOffset.UTC);
    }

    public OffsetDateTime getEndTimestamp() {
        return OffsetDateTime.ofInstant(Instant.ofEpochSecond(end), ZoneOffset.UTC);
    }

    public String getId() {
        return id;
    }
}
