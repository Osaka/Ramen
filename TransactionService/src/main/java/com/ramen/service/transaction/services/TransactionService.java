package com.ramen.service.transaction.services;

import com.ramen.service.transaction.model.Transaction;
import com.ramen.service.transaction.repository.ITransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

@Service
public class TransactionService implements ITransactionService {

    private final ITransactionRepository transactionRepository;

    @Autowired
    public TransactionService(ITransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    /**
     * @param customerId
     * @param merchantId
     * @param tokenId
     * @param amount
     * @param timestamp
     * @return Either the new transaction or null
     * @author Alexander Armstrong
     */
    @Override
    public Transaction createTransaction(UUID customerId, UUID merchantId, UUID tokenId, BigDecimal amount, OffsetDateTime timestamp, boolean refunded) {

        Transaction newTransaction = new Transaction(customerId, merchantId, tokenId, amount, timestamp, refunded);
        return transactionRepository.saveAndFlush(newTransaction);
    }

    /**
     * @param id of the Transaction
     * @return the requested transaction or null.
     * @author Morten Telling
     */
    @Override
    public Transaction getTransaction(UUID id) {
        if (id == null) return null;

        return transactionRepository.findById(id).orElse(null);
    }

    /**
     * @param id
     * @return if the transaction has been refunded
     * @author Tobias Lindstrøm
     */
    @Override
    public boolean refundTransaction(UUID id) {
        return transactionRepository.refundTransaction(id) == 1;
    }
}
