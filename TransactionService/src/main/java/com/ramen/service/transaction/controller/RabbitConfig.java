package com.ramen.service.transaction.controller;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
* @author Lasse Spindler Mørk (s154235)
*/
@Configuration
public class RabbitConfig {

    public static final String
            CUSTOMER_REPORT_QUEUE = "transaction.customerReport",
            MERCHANT_REPORT_QUEUE = "transaction.merchantReport",
            LOG_TRANSACTION_QUEUE = "transaction.log",
            REFUND_TRANSACTION_QUEUE = "transaction.refund",
            EXCHANGE = "transaction";

    public static class ServerConfig {
        @Bean
        public Queue queueMerchantReport() {
            return new Queue(MERCHANT_REPORT_QUEUE, true);
        }

        @Bean
        public Queue queueCustomerReport() {
            return new Queue(CUSTOMER_REPORT_QUEUE, true);
        }

        @Bean
        public Queue queueLogTransaction() {
            return new Queue(LOG_TRANSACTION_QUEUE, true);
        }

        @Bean
        public Queue queueRefundTransaction() {
            return new Queue(REFUND_TRANSACTION_QUEUE, true);
        }

        @Bean
        public DirectExchange exchange() {
            return new DirectExchange(EXCHANGE);
        }

        @Bean
        public Binding bindingTransfer(DirectExchange exchange, Queue queueCustomerReport) {
            return BindingBuilder.bind(queueCustomerReport).to(exchange).with(queueCustomerReport.getName());
        }

        @Bean
        public Binding bindingCreate(DirectExchange exchange, Queue queueLogTransaction) {
            return BindingBuilder.bind(queueLogTransaction).to(exchange).with(queueLogTransaction.getName());
        }

        @Bean
        public Binding bindingAccount(DirectExchange exchange, Queue queueMerchantReport) {
            return BindingBuilder.bind(queueMerchantReport).to(exchange).with(queueMerchantReport.getName());
        }

        @Bean
        public Binding bindingRefund(DirectExchange exchange, Queue queueRefundTransaction) {
            return BindingBuilder.bind(queueRefundTransaction).to(exchange).with(queueRefundTransaction.getName());
        }

        @Bean
        public RabbitServer server() {
            return new RabbitServer();
        }
    }
}
