package com.ramen.service.transaction.repository;

import com.ramen.service.transaction.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface ITransactionRepository extends JpaRepository<Transaction, UUID> {

    List<Transaction> getByTimestampBetweenAndCustomerId(OffsetDateTime from, OffsetDateTime to, UUID customerId);

    List<Transaction> getByTimestampBetweenAndMerchantId(OffsetDateTime from, OffsetDateTime to, UUID merchantId);

    /**
     * Custom query for refunding a token
     *
     * @param transactionId id of the token to refund
     * @return number of affected rows in the db
     * @author Tobias Lindstrøm (s15335)
     */
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("Update Transaction t set t.refunded=true Where t.id=:transactionId And t.refunded=false")
    int refundTransaction(@Param("transactionId") UUID transactionId);

}
