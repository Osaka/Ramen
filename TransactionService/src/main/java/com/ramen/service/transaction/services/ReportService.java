package com.ramen.service.transaction.services;

import com.google.gson.Gson;
import com.ramen.service.transaction.model.Transaction;
import com.ramen.service.transaction.repository.ITransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ReportService implements IReportService {

    private final ITransactionRepository repository;

    @Autowired
    public ReportService(ITransactionRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Transaction> createCustomerReport(OffsetDateTime from, OffsetDateTime to, UUID customerId) {
        return repository.getByTimestampBetweenAndCustomerId(from, to, customerId);
    }

    @Override
    public List<Transaction> createMerchantReport(OffsetDateTime from, OffsetDateTime to, UUID merchantId) {
        return repository.getByTimestampBetweenAndMerchantId(from, to, merchantId).stream()
                .peek(transaction -> transaction.setCustomerId(null))
                .collect(Collectors.toList());
    }
}
