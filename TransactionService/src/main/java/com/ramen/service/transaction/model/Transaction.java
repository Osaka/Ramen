package com.ramen.service.transaction.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private UUID customerId;

    private UUID merchantId;

    private UUID tokenId;

    private BigDecimal amount;

    private OffsetDateTime timestamp;

    private boolean refunded;

    public Transaction() {
    }

    public Transaction(UUID customerId, UUID merchantId, UUID tokenId, BigDecimal amount, OffsetDateTime timestamp) {
        this.customerId = customerId;
        this.merchantId = merchantId;
        this.tokenId = tokenId;
        this.timestamp = timestamp;
        this.amount = amount;
        this.refunded = false;
    }

    public Transaction(UUID customerId, UUID merchantId, UUID tokenId, BigDecimal amount, OffsetDateTime timestamp, boolean refunded) {
        this.customerId = customerId;
        this.merchantId = merchantId;
        this.tokenId = tokenId;
        this.timestamp = timestamp;
        this.amount = amount;
        this.refunded = refunded;
    }

    public UUID getId() {
        return this.id;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public UUID getMerchantId() {
        return merchantId;
    }

    public UUID getTokenId() {
        return tokenId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Transaction) {
            return this.id.equals(((Transaction) other).getId());
        }
        return false;
    }

    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    public void setCustomerId(UUID id) {
        this.customerId = id;
    }

    public boolean getRefunded() {
        return refunded;
    }
}
