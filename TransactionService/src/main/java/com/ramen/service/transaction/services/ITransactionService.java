package com.ramen.service.transaction.services;

import com.ramen.service.transaction.model.Transaction;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

public interface ITransactionService {

    Transaction createTransaction(UUID customerId, UUID merchantId, UUID tokenId, BigDecimal amount, OffsetDateTime timestamp, boolean refunded);

    Transaction getTransaction(UUID id);

    boolean refundTransaction(UUID id);

}
