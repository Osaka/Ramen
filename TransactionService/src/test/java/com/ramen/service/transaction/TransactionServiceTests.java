package com.ramen.service.transaction;

import com.ramen.service.transaction.model.Transaction;
import com.ramen.service.transaction.repository.ITransactionRepository;
import com.ramen.service.transaction.services.ITransactionService;
import com.ramen.service.transaction.time.Timestamp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

import static java.math.BigDecimal.ONE;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionServiceTests {

    @Autowired
    ITransactionRepository repository;

    @Autowired
    ITransactionService service;

    @Before
    public void setup() {
        repository.deleteAll();
        repository.flush();
    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void savingTransactionTest() {
        // Setup
        Transaction expected = repository.saveAndFlush(new Transaction(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                ONE,
                Timestamp.now()));

        // Act
        List<Transaction> list = repository.findAll();
        Transaction actual = list.get(0);

        // Assert
        assertEquals(1, list.size());
        assertEquals(expected.getId(), actual.getId());
    }

    @Test
    public void createTransaction() {
        // Setup
        Transaction newTransaction = service.createTransaction(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                ONE,
                Timestamp.now(), false);

        // Assert
        assertNotNull(newTransaction.getId());
        assertThat(newTransaction.getId(), not(is("")));
        assertThat(repository.findById(newTransaction.getId()).orElse(null),
                is(newTransaction));
    }

    @Test
    public void getExistingTransaction() {
        // Setup
        Transaction transaction1 = service.createTransaction(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                ONE,
                Timestamp.now(), false);
        UUID transactionId = transaction1.getId();

        // Act
        Transaction transaction2 = service.getTransaction(transactionId);

        // Assert
        assertThat(transaction2, is(equalTo(transaction1)));
    }

    @Test
    public void getNonExistentTransaction() {
        // Act
        Transaction transaction = service.getTransaction(UUID.randomUUID());

        // Assert
        assertThat(transaction, is(equalTo(null)));
    }

    @Test
    public void getCustomerTransactionsTest() {
        // Setup
        Transaction transaction = service.getTransaction(UUID.randomUUID());


    }

}

