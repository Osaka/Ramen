package com.ramen.service.transaction;

import com.ramen.service.transaction.model.Transaction;
import com.ramen.service.transaction.repository.ITransactionRepository;
import com.ramen.service.transaction.services.IReportService;
import com.ramen.service.transaction.time.Timestamp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import static java.math.BigDecimal.ONE;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 * @author Lasse Spindler Mørk (s154235)
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ReportServiceTests {

    @Autowired
    ITransactionRepository repository;

    @Autowired
    IReportService service;

    @Before
    public void setup() {
        repository.deleteAll();
        repository.flush();
    }

    @After
    public void cleanup() {
        repository.deleteAll();
        repository.flush();
    }

    private OffsetDateTime daysFromNow(int days) {
        return Timestamp.now().plusDays(days);
    }


    @Test
    public void getCustomerTransactionWithinGivenTimeTest() {
        // Setup
        UUID customerId = UUID.randomUUID();
        OffsetDateTime fromTime = daysFromNow(-5);
        OffsetDateTime toTime = daysFromNow(5);
        Transaction expected = repository.saveAndFlush(new Transaction(
                customerId,
                UUID.randomUUID(),
                UUID.randomUUID(),
                ONE,
                Timestamp.now()));

        // Act
        List<Transaction> list = service.createCustomerReport(fromTime, toTime, customerId);

        // Assert
        assertThat(list, hasSize(1));
        assertThat(list.get(0), is(equalTo(expected)));
    }

    @Test
    public void dontGetCustomerTransactionBeforeGivenTimeTest() {
        // Setup
        UUID customerId = UUID.randomUUID();
        OffsetDateTime fromTime = daysFromNow(5);
        OffsetDateTime toTime = daysFromNow(10);
        repository.saveAndFlush(new Transaction(
                customerId,
                UUID.randomUUID(),
                UUID.randomUUID(),
                ONE,
                Timestamp.now()));

        // Act
        List<Transaction> list = service.createCustomerReport(fromTime, toTime, customerId);

        // Assert
        assertThat(list, hasSize(0));
    }

    @Test
    public void dontGetCustomerTransactionAfterGivenTimeTest() {
        // Setup
        UUID customerId = UUID.randomUUID();
        OffsetDateTime fromTime = daysFromNow(-10);
        OffsetDateTime toTime = daysFromNow(-5);
        repository.saveAndFlush(new Transaction(
                customerId,
                UUID.randomUUID(),
                UUID.randomUUID(),
                ONE,
                Timestamp.now()));

        // Act
        List<Transaction> list = service.createCustomerReport(fromTime, toTime, customerId);

        // Assert
        assertThat(list, hasSize(0));
    }

    @Test
    public void getMerchantTransactionWithinGivenTimeTest() {
        // Setup
        UUID merchantId = UUID.randomUUID();
        OffsetDateTime fromTime = daysFromNow(-5);
        OffsetDateTime toTime = daysFromNow(5);
        Transaction expected = repository.saveAndFlush(new Transaction(
                UUID.randomUUID(),
                merchantId,
                UUID.randomUUID(),
                ONE,
                Timestamp.now()));

        // Act
        List<Transaction> list = service.createMerchantReport(fromTime, toTime, merchantId);

        // Assert
        assertThat(list, hasSize(1));
        assertThat(list.get(0), is(equalTo(expected)));
    }

    @Test
    public void dontGetMerchantTransactionBeforeGivenTimeTest() {
        // Setup
        UUID merchantId = UUID.randomUUID();
        OffsetDateTime fromTime = daysFromNow(5);
        OffsetDateTime toTime = daysFromNow(10);
        repository.saveAndFlush(new Transaction(
                UUID.randomUUID(),
                merchantId,
                UUID.randomUUID(),
                ONE,
                Timestamp.now()));

        // Act
        List<Transaction> list = service.createMerchantReport(fromTime, toTime, merchantId);

        // Assert
        assertThat(list, hasSize(0));
    }

    @Test
    public void dontGetMerchantTransactionAfterGivenTimeTest() {
        // Setup
        UUID merchantId = UUID.randomUUID();
        OffsetDateTime fromTime = daysFromNow(-10);
        OffsetDateTime toTime = daysFromNow(-5);
        repository.saveAndFlush(new Transaction(
                UUID.randomUUID(),
                merchantId,
                UUID.randomUUID(),
                ONE,
                Timestamp.now()));

        // Act
        List<Transaction> list = service.createMerchantReport(fromTime, toTime, merchantId);

        // Assert
        assertThat(list, hasSize(0));
    }

    @Test
    public void dontGetCustomerIdForMerchantReportTest() {
        // Setup
        UUID customerId = UUID.randomUUID();
        UUID merchantId = UUID.randomUUID();
        OffsetDateTime fromTime = daysFromNow(-5);
        OffsetDateTime toTime = daysFromNow(5);
        repository.saveAndFlush(new Transaction(
                customerId,
                merchantId,
                UUID.randomUUID(),
                ONE,
                Timestamp.now()));

        // Act
        UUID merchantView = service.createMerchantReport(fromTime, toTime, merchantId)
                .get(0).getCustomerId();
        UUID customerView = service.createCustomerReport(fromTime, toTime, customerId)
                .get(0).getCustomerId();

        // Assert
        assertThat(merchantView, is(nullValue()));
        assertThat(customerView, is(equalTo(customerId)));
    }
}
