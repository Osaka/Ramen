package com.ramen.service.payment.library;

import com.ramen.service.payment.models.PaymentInput;

import java.util.UUID;

public interface ITransactionService {

    String logTransaction(String customerId, PaymentInput paymentInput);

    String refundTransaction(UUID transactionId);

}
