package com.ramen.service.payment.models;


import java.math.BigDecimal;

public class PaymentInput {
    public BigDecimal amount;
    public String tokenId;
    public String merchantId;
    public boolean refunded = false;
}