package com.ramen.service.payment.library;

public interface IMerchantService {

    String getAccount(String merchantId);
}
