package com.ramen.service.payment.controller;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * Service for sending messages to the bank service
 *
 * @author Tobias Lindstrøm (s153356)
 */
@Configuration
public class RabbitConfig {

    public final static String
            QUEUE_PAYMENY_PAY = "payment.pay",
            QUEUE_PAYMENT_REFUND = "payment.refund";

    public static class ClientConfig {

        @Bean
        @Qualifier("bank")
        public DirectExchange bankExchange() {
            return new DirectExchange("bank");
        }

        @Bean
        @Qualifier("token")
        public DirectExchange tokenExchange() {
            return new DirectExchange("token");
        }

        @Bean
        @Qualifier("customer")
        public DirectExchange customerExchange() {
            return new DirectExchange("customer");
        }

        @Bean
        @Qualifier("merchant")
        public DirectExchange merchantExchange() {
            return new DirectExchange("merchant");
        }

        @Bean
        @Qualifier("transaction")
        public DirectExchange transactionExchange() {
            return new DirectExchange("transaction");
        }
    }

    public static class ServerConfig {
        @Bean
        public Queue queuePay() {
            return new Queue(QUEUE_PAYMENY_PAY, true);
        }

        @Bean
        public Queue queueRefund() {
            return new Queue(QUEUE_PAYMENT_REFUND, true);
        }

        @Primary
        @Bean
        @Qualifier("payment")
        public DirectExchange paymentExchange() {
            return new DirectExchange("payment");
        }

        @Bean
        public Binding bindingPay(DirectExchange paymentExchange, Queue queuePay) {
            return BindingBuilder.bind(queuePay).to(paymentExchange).with(queuePay.getName());
        }

        @Bean
        public Binding bindingRefund(DirectExchange paymentExchange, Queue queueRefund) {
            return BindingBuilder.bind(queueRefund).to(paymentExchange).with(queueRefund.getName());
        }

        @Bean
        public RabbitServer server() {
            return new RabbitServer();
        }
    }
}
