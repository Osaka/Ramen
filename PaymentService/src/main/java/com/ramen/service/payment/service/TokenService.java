package com.ramen.service.payment.service;

import com.ramen.service.payment.library.ITokenService;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Service for sending messages to the token service
 *
 * @author Tobias Lindstrøm (s15335)
 */
@Service
public class TokenService implements ITokenService {

    private final static String QUEUE_TOKEN_CONSUME = "token.consume";

    @Autowired
    private RabbitTemplate template;

    @Autowired
    @Qualifier("token")
    private DirectExchange exchange;

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    @Override
    public String consumeToken(String tokenId) {
        return send(QUEUE_TOKEN_CONSUME, tokenId);
    }
}
