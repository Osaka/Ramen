package com.ramen.service.payment.library;

public interface ICustomerService {

    String getAccount(String customerId);
}
