package com.ramen.service.payment.library;

public interface ITokenService {

    /**
     * Attempts to consume a token and returns the id of the user whose token has been consumed.
     *
     * @param tokenId
     * @return the ID of the user who owns the given token
     * @author Alexander Armstrong
     */
    String consumeToken(String tokenId);

}
