package com.ramen.service.payment.controller;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.ramen.service.payment.library.*;
import com.ramen.service.payment.models.GenericResponse;
import com.ramen.service.payment.models.PaymentInput;
import com.ramen.service.payment.models.Transaction;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.UUID;

import static com.ramen.service.payment.models.GenericResponse.failure;
import static com.ramen.service.payment.models.GenericResponse.fromJson;

public class RabbitServer {

    @Autowired
    private IBankService bankService;

    @Autowired
    private ITokenService tokenService;

    @Autowired
    private ICustomerService customerService;

    @Autowired
    private IMerchantService merchantService;

    @Autowired
    private ITransactionService transactionService;

    private Gson gson = new Gson();

    /**
     * Send payment from usertoken to merchant id
     *
     * @param jsonString json object of type PaymentInput
     * @return Id of the transaction logged
     * @author Tobias Lindstrøm (s153365)
     */
    @RabbitListener(queues = RabbitConfig.QUEUE_PAYMENY_PAY)
    public String pay(String jsonString) {
        PaymentInput paymentInput;

        try {
            paymentInput = gson.fromJson(jsonString, PaymentInput.class);
            if (paymentInput.amount == null || paymentInput.tokenId == null || paymentInput.merchantId == null) {
                return failure("Invalid arguments").toJson();
            }
        } catch (JsonParseException e) {
            return failure(e).toJson();
        }

        if (paymentInput.amount.compareTo(BigDecimal.ZERO) <= 0)
            return failure("Need to send a non zero positive amount").toJson();

        GenericResponse customerIdResponse = fromJson(tokenService.consumeToken(paymentInput.tokenId));

        if (customerIdResponse.isFailure())
            return customerIdResponse.toJson();

        GenericResponse customerAccountResponse = fromJson(customerService.getAccount(customerIdResponse.getResult()));

        if (customerAccountResponse.isFailure())
            return customerAccountResponse.toJson();

        GenericResponse merchantAccountResponse = fromJson(merchantService.getAccount(paymentInput.merchantId));

        if (merchantAccountResponse.isFailure())
            return merchantAccountResponse.toJson();

        String customerAccount = customerAccountResponse.getResult();
        String merchantAccount = merchantAccountResponse.getResult();

        String desc = String.format("Payment from: %s, to %s", customerAccount, merchantAccount);

        GenericResponse bankResponse = fromJson(bankService.transferMoney(paymentInput.amount, customerAccount, merchantAccount, desc));

        if (bankResponse.isFailure())
            return bankResponse.toJson();

        String customerId = gson.fromJson(customerIdResponse.getResult(), String.class);

        String response = transactionService.logTransaction(customerId, paymentInput);
        return response;
    }

    /**
     * Refund transaction
     *
     * @param input transaction id
     * @return Id of the refund transaction
     * @author Tobias Lindstrom (s153365)
     */
    @RabbitListener(queues = RabbitConfig.QUEUE_PAYMENT_REFUND)
    public String refund(String input) {
        UUID transactionId = gson.fromJson(input, UUID.class);

        GenericResponse refundTransactionResponse = fromJson(transactionService.refundTransaction(transactionId));

        if (refundTransactionResponse.isFailure())
            return refundTransactionResponse.toJson();

        Transaction refundedTransaction = gson.fromJson(refundTransactionResponse.getResult(), Transaction.class);

        GenericResponse customerAccountResponse = fromJson(
                customerService.getAccount(refundedTransaction.customerId.toString()));

        if (customerAccountResponse.isFailure())
            return customerAccountResponse.toJson();

        GenericResponse merchantAccountResponse = fromJson(
                merchantService.getAccount(refundedTransaction.merchantId.toString()));

        if (merchantAccountResponse.isFailure())
            return merchantAccountResponse.toJson();

        String customerAccount = customerAccountResponse.getResult();
        String merchantAccount = merchantAccountResponse.getResult();

        String desc = String.format("Refund from: %s, to %s", merchantAccount, customerAccount);

        GenericResponse bankResponse = fromJson(bankService.transferMoney(
                refundedTransaction.amount, merchantAccount, customerAccount, desc));

        if (bankResponse.isFailure())
            return bankResponse.toJson();

        PaymentInput paymentInput = new PaymentInput();
        paymentInput.refunded = true;
        paymentInput.amount = refundedTransaction.amount;
        paymentInput.tokenId = refundedTransaction.tokenId.toString();
        paymentInput.merchantId = refundedTransaction.merchantId.toString();

        String response = transactionService.logTransaction(refundedTransaction.customerId.toString(), paymentInput);
        return response;
    }
}
