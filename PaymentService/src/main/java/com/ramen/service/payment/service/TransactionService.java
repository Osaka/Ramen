package com.ramen.service.payment.service;

import com.google.gson.Gson;
import com.ramen.service.payment.library.ITransactionService;
import com.ramen.service.payment.models.PaymentInput;
import com.ramen.service.payment.models.Transaction;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Service for sending messages to the transaction service
 *
 * @author Tobias Lindstrøm (s15335)
 */
@Service
public class TransactionService implements ITransactionService {

    private final static String QUEUE_LOG_TRANSACTION = "transaction.log";
    private final static String QUEUE_REFUND_TRANSACTION = "transaction.refund";

    private Gson gson = new Gson();

    @Autowired
    private RabbitTemplate template;

    @Autowired
    @Qualifier("transaction")
    private DirectExchange exchange;

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    @Override
    public String logTransaction(String customerId, PaymentInput paymentInput) {
        Transaction transaction = new Transaction();
        transaction.customerId = UUID.fromString(customerId);
        transaction.merchantId = UUID.fromString(paymentInput.merchantId);
        transaction.tokenId = UUID.fromString(paymentInput.tokenId);
        transaction.amount = paymentInput.amount;
        transaction.timestamp = OffsetDateTime.now();
        transaction.refunded = paymentInput.refunded;

        return send(QUEUE_LOG_TRANSACTION, gson.toJson(transaction));
    }

    @Override
    public String refundTransaction(UUID transactionId) {
        return send(QUEUE_REFUND_TRANSACTION, gson.toJson(transactionId));
    }
}
