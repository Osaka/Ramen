package com.ramen.service.payment.service;

import com.ramen.service.payment.library.IMerchantService;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Service for sending messages to the merchant service
 *
 * @author Tobias Lindstrøm (s15335)
 */
@Service
public class MerchantService implements IMerchantService {

    private final static String QUEUE_MERCHANT_GET_ACCOUNT = "merchant.getAccount";

    @Autowired
    private RabbitTemplate template;

    @Autowired
    @Qualifier("merchant")
    private DirectExchange exchange;

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    @Override
    public String getAccount(String merchantId) {
        return send(QUEUE_MERCHANT_GET_ACCOUNT, merchantId);
    }
}
