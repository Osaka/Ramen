package com.ramen.service.payment.service;

import com.ramen.service.payment.library.ICustomerService;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Service for sending messages to the customer service
 *
 * @author Tobias Lindstrøm (s15335)
 */
@Service
public class CustomerService implements ICustomerService {

    private final static String QUEUE_CUSTOMER_GET_ACCOUNT = "customer.getAccount";

    @Autowired
    private RabbitTemplate template;

    @Autowired
    @Qualifier("customer")
    private DirectExchange exchange;

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    @Override
    public String getAccount(String customerId) {
        return send(QUEUE_CUSTOMER_GET_ACCOUNT, customerId);
    }
}
