package com.ramen.service.payment.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ramen.service.payment.library.IBankService;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Service for sending messages to the bank service
 *
 * @author Tobias Lindstrøm (s15335)
 */
@Service
public class BankService implements IBankService {

    private Gson gson = new Gson();

    private final static String QUEUE_BANK_TRANSFER = "bank.transfer";

    @Autowired
    private RabbitTemplate template;

    @Autowired
    @Qualifier("bank")
    private DirectExchange exchange;

    private String send(String queueName, String message) {
        return (String) template.convertSendAndReceive(exchange.getName(), queueName, message);
    }

    @Override
    public String transferMoney(BigDecimal amount, String fromAccount, String toAccount, String message) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("amount", gson.toJson(amount));
        jsonObject.addProperty("fromAccount", fromAccount);
        jsonObject.addProperty("toAccount", toAccount);
        jsonObject.addProperty("description", message);

        return send(QUEUE_BANK_TRANSFER, jsonObject.toString());
    }
}
