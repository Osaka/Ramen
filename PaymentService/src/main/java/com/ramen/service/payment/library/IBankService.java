package com.ramen.service.payment.library;

import java.math.BigDecimal;

public interface IBankService {

    String transferMoney(BigDecimal amount, String fromAccount, String toAccount, String message);

}
