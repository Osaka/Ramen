package com.ramen.service.payment.models;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

public class Transaction {
    public UUID customerId, merchantId, tokenId;
    public BigDecimal amount;
    public OffsetDateTime timestamp;
    public boolean refunded = false;
}
