package com.ramen.service.merchant;

import com.ramen.service.merchant.model.Merchant;
import com.ramen.service.merchant.repository.IMerchantRepository;
import com.ramen.service.merchant.services.IMerchantService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MerchantApplicationTests {

    @Autowired
    private IMerchantRepository merchantRepository;

    @Autowired
    private IMerchantService merchantService;

    private UUID testAccountId = UUID.randomUUID();

    /**
     * @author Alexander Armstrong
     */
    @Test
    public void addMerchantTest() {
        Merchant newMerchant = new Merchant(testAccountId);
        merchantRepository.save(newMerchant);

        Assert.assertThat(merchantRepository.findOne(Example.of(newMerchant)).get(), is(newMerchant));
    }

    /**
     * @author Alexander Armstrong
     */
    @Test
    public void createMerchantTest() {
        Merchant merchant = merchantService.createMerchant(testAccountId);

        Assert.assertNotNull(merchant.getId());
        Assert.assertThat(merchant.getId(), not(is("")));
        Assert.assertThat(
                merchantRepository
                        .findById(merchant.getId())
                        .orElse(null),
                is(merchant));
    }

    /**
     * @author Alexander Armstrong
     */
    @Test
    public void deleteMerchantExistsTest() {
        Merchant merchant = new Merchant(testAccountId);
        merchantRepository.save(merchant);

        assertTrue(merchantService.deleteMerchant(merchant.getId()));
        Assert.assertThat(
                merchantRepository
                        .findById(merchant.getId())
                        .isPresent(),
                is(false));
    }

    @Test
    public void deleteMerchantDoesNotExistTest() {
        assertFalse(merchantService.deleteMerchant(UUID.randomUUID()));
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @Test
    public void updateExistingMerchant() {
        // Setup
        Merchant merchant = merchantService.createMerchant(UUID.randomUUID());
        UUID newAccountNumber = UUID.randomUUID();

        // Act
        merchant.setAccountNumber(newAccountNumber);
        Merchant updatedMerchant = merchantService.updateMerchant(merchant);

        // Assert
        assertEquals(merchant.getId(), updatedMerchant.getId());
        assertEquals(merchant.getAccountNumber(), updatedMerchant.getAccountNumber());
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @Test
    public void updateNonExistingMerchant() {
        // Setup
        Merchant merchant = merchantService.createMerchant(UUID.randomUUID());
        Boolean merchantDeleted = merchantService.deleteMerchant(merchant.getId());

        // Act
        Merchant updatedMerchant = merchantService.updateMerchant(merchant);

        // Assert
        assertTrue(merchantDeleted);
        assertNull(updatedMerchant);
    }
}

