package com.ramen.service.merchant.services;

import com.ramen.service.merchant.model.Merchant;
import com.ramen.service.merchant.repository.IMerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class MerchantService implements IMerchantService {

    private final IMerchantRepository merchantRepository;

    @Autowired
    public MerchantService(IMerchantRepository merchantRepository) {
        this.merchantRepository = merchantRepository;
    }

    /**
     * @param accountNumber for the merchant
     * @return the created Merchant object
     * @author Morten Telling
     */
    @Override
    public Merchant createMerchant(UUID accountNumber) {
        if (accountNumber == null) return null;

        Merchant newMerchant = new Merchant(accountNumber);
        return merchantRepository.saveAndFlush(newMerchant);
    }


    @Override
    public Merchant createMerchant(Merchant original) {
        return createMerchant(original.getAccountNumber());
    }

    /**
     * @param merchant object with updated values
     * @return updated merchant object
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @Override
    public Merchant updateMerchant(Merchant merchant) {
        // Make sure merchant already exists
        if (getMerchant(merchant.getId()) == null) {
            return null;
        }

        // Update merchant
        return merchantRepository.saveAndFlush(merchant);
    }

    /**
     * @param id of the merchant to be deleted
     * @return boolean describing whether the deletion succeeded.
     * @author Alexander Armstrong
     */
    @Override
    public boolean deleteMerchant(UUID id) {
        if (id == null) return false;

        Optional<Merchant> merchantToBeDeleted = merchantRepository.findById(id);
        if (merchantToBeDeleted.isPresent()) {
            merchantRepository.delete(merchantToBeDeleted.get());
            return true;
        }
        return false;
    }

    /**
     * @param id
     * @return the requested merchant if it exists otherwise null
     * @author Morten Telling
     */
    @Override
    public Merchant getMerchant(UUID id) {
        if (id == null) return null;

        Optional<Merchant> merchant = merchantRepository.findById(id);
        return merchant.orElse(null);
    }

}
