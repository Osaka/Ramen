package com.ramen.service.merchant.services;

import com.ramen.service.merchant.model.Merchant;

import java.util.UUID;


public interface IMerchantService {

    Merchant createMerchant(UUID accountNumber);

    Merchant updateMerchant(Merchant merchant);

    boolean deleteMerchant(UUID id);

    Merchant getMerchant(UUID id);

    Merchant createMerchant(Merchant newMerchant);
}
