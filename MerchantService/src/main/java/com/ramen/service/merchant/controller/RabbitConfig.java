package com.ramen.service.merchant.controller;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Morten Telling
 */
@Configuration
public class RabbitConfig {
    public final static String
            QUEUE_MERCHANT_GETACCOUNT = "merchant.getAccount",
            QUEUE_MERCHANT_CREATE = "merchant.create",
            QUEUE_MERCHANT_UPDATE = "merchant.update",
            QUEUE_MERCHANT_GET = "merchant.get",
            QUEUE_MERCHANT_DELETE = "merchant.delete";

    public static class ServerConfig {

        @Bean
        public Queue queueGetAccount() {
            return new Queue(QUEUE_MERCHANT_GETACCOUNT, true);
        }

        @Bean
        public Queue queueCreate() {
            return new Queue(QUEUE_MERCHANT_CREATE, true);
        }

        @Bean
        public Queue queueUpdate() {
            return new Queue(QUEUE_MERCHANT_UPDATE, true);
        }

        @Bean
        public Queue queueGet() {
            return new Queue(QUEUE_MERCHANT_GET, true);
        }

        @Bean
        public Queue queueDelete() {
            return new Queue(QUEUE_MERCHANT_DELETE, true);
        }

        @Bean
        public DirectExchange exchange() {
            return new DirectExchange("merchant");
        }

        @Bean
        public Binding bindingGetAccount(DirectExchange exchange, Queue queueGetAccount) {
            return BindingBuilder.bind(queueGetAccount).to(exchange).with(queueGetAccount.getName());
        }

        @Bean
        public Binding bindingCreate(DirectExchange exchange, Queue queueCreate) {
            return BindingBuilder.bind(queueCreate).to(exchange).with(queueCreate.getName());
        }

        @Bean
        public Binding bindingUpdate(DirectExchange exchange, Queue queueUpdate) {
            return BindingBuilder.bind(queueUpdate).to(exchange).with(queueUpdate.getName());
        }

        @Bean
        public Binding bindingGet(DirectExchange exchange, Queue queueGet) {
            return BindingBuilder.bind(queueGet).to(exchange).with(queueGet.getName());
        }

        @Bean
        public Binding bindingDelete(DirectExchange exchange, Queue queueDelete) {
            return BindingBuilder.bind(queueDelete).to(exchange).with(queueDelete.getName());
        }

        @Bean
        public RabbitServer server() {
            return new RabbitServer();
        }
    }
}
