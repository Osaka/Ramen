package com.ramen.service.merchant.controller;

import com.google.gson.JsonParseException;
import com.ramen.service.merchant.model.Merchant;
import com.ramen.service.merchant.services.IMerchantService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static com.ramen.service.merchant.model.GenericResponse.*;

public class RabbitServer {

    @Autowired
    private IMerchantService service;

    /**
     * @param id response the given merchant
     * @return id response the account or null
     * @author Morten Telling
     * Returns the account id for a merchant or null if the id is incorrect
     */
    @RabbitListener(queues = RabbitConfig.QUEUE_MERCHANT_GETACCOUNT)
    public String getAccount(String id) {
        UUID merchantId;
        try {
            merchantId = UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            return failure(e).toJson();
        }

        Merchant merchant = service.getMerchant(merchantId);

        if (merchant == null || merchant.getAccountNumber() == null) {
            return failure("Invalid merchant ID").toJson();
        }

        return response(merchant.getAccountNumber().toString()).toJson();

    }

    /**
     * @param merchant formatted as json string
     * @return The created merchant as json or an error message.
     * @author Alexander Armstrong
     * Creates a new merchant in the microservice
     */
    @RabbitListener(queues = RabbitConfig.QUEUE_MERCHANT_CREATE)
    public String createMerchant(String merchant) {
        Merchant newMerchant;
        try {
            newMerchant = gson.fromJson(merchant, Merchant.class);
        } catch (JsonParseException e) {
            return failure(e).toJson();
        }

        Merchant createdMerchant = service.createMerchant(newMerchant);

        return response(createdMerchant).toJson();
    }

    /**
     * @param merchant formatted as json string
     * @return The updated merchant as json or an error message
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @RabbitListener(queues = RabbitConfig.QUEUE_MERCHANT_UPDATE)
    public String updateMerchant(String merchant) {
        // Parse received merchant
        Merchant updatedMerchant;
        try {
            updatedMerchant = gson.fromJson(merchant, Merchant.class);
        } catch (JsonParseException e) {
            return failure(e).toJson();
        }

        // Check if merchant exists
        Merchant existingMerchant = service.getMerchant(updatedMerchant.getId());
        if (existingMerchant == null) {
            return failure("Merchant does not exist").toJson();
        }

        // Update existing merchant
        if (updatedMerchant.getAccountNumber() != null) {
            existingMerchant.setAccountNumber(updatedMerchant.getAccountNumber());
        }

        return response(service.updateMerchant(existingMerchant)).toJson();
    }

    /**
     * @param merchantId formatted as json string
     * @return error code and message indicating if the merchant was deleted
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @RabbitListener(queues = RabbitConfig.QUEUE_MERCHANT_DELETE)
    public String deleteMerchant(String merchantId) {
        // Parse received merchant id
        UUID merchantIdToDelete;
        try {
            merchantIdToDelete = gson.fromJson(merchantId, UUID.class);
        } catch (JsonParseException e) {
            return failure(e).toJson();
        }

        // Delete merchant
        if (!service.deleteMerchant(merchantIdToDelete)) {
            return failure("Merchant does not exist").toJson();
        }

        return response("Deleted merchant").toJson();
    }

    /**
     * @author Alexander Armstrong
     * @param id of the merchant
     * @return the merchant object
     */
    @RabbitListener(queues = RabbitConfig.QUEUE_MERCHANT_GET)
    public String getMerchant(String id) {
        UUID merchantId;
        try {
            merchantId = UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            return failure(e).toJson();
        }

        Merchant merchant = service.getMerchant(merchantId);

        return merchant != null ? response(merchant).toJson() : failure("Merchant does not exist").toJson();
    }
}
