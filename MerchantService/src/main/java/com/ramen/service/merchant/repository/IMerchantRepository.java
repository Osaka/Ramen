package com.ramen.service.merchant.repository;

import com.ramen.service.merchant.model.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IMerchantRepository extends JpaRepository<Merchant, UUID> {
}
