package com.ramen.service.merchant.model;

import javax.persistence.*;
import java.util.UUID;

/**
 * @author Tobias Lindstrøm (s15335)
 */
@Entity
public class Merchant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(nullable = false)
    private UUID accountNumber;

    public Merchant() {
    }

    public Merchant(UUID accountNumber) {
        this.accountNumber = accountNumber;
    }

    public UUID getId() {
        return this.id;
    }

    public UUID getAccountNumber() {
        return this.accountNumber;
    }

    public void setAccountNumber(UUID accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Merchant) {
            return this.id.equals(((Merchant) other).getId());
        }
        return false;
    }

}
