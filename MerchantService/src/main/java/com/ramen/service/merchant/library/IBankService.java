package com.ramen.service.merchant.library;

public interface IBankService {

    String createAccount(String firstName, String lastName, String cprNumber);

}