Feature: Rest test of transaction logs
  # Author: Mathias Ennegaard Asmussen (s154219)
  Scenario: Retrieving a merchant transaction report log
    Given a valid customer with 10 money and 1 tokens
    And a valid merchant with 10 money
    And a transaction of 5 is made
    When a merchant ask for a transaction report log between today and tomorrow
    Then a transaction log of length 1 should be returned
    And that transaction should contain the customer token
    And that transaction should have an amount of 5 money

  # Author: Lasse Spindler Mørk (s154235)
  Scenario: Retrieving a customer transaction report log
    Given a valid customer with 10 money and 1 tokens
    And a valid merchant with 10 money
    And a transaction of 5 is made
    When a customer ask for a transaction report log between today and tomorrow
    Then a transaction log of length 1 should be returned
    And that transaction should contain the customer token
    And that transaction should have an amount of 5 money
