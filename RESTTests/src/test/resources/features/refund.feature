Feature: Merchant Gateway: Refund

# Author: Tobias Lindstrøm (s153365)
  Scenario: Refunding a transaction
    Given a valid customer with 100 money and 1 tokens
    And a valid merchant
    And a transaction of 100 is made
    When a refund is requested
    Then customer account should have 100 money
    And merchant account should have 0 money