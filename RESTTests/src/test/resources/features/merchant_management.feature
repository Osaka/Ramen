Feature: Merchant management
  # Author: Mathies Svarrer-Lanthen (s154070)
  Scenario: Create new merchant
    When a new merchant is created
    Then a new merchant should be created


  # Author: Mathies Svarrer-Lanthen (s154070)
  Scenario: Update existing merchant
    Given a valid merchant
    When I update the merchant information
    Then the merchant information should be updated

  Scenario: Update non existing merchant
    Given an invalid merchant
    When I update the merchant information
    Then I should receive an error


  # Author: Mathies Svarrer-Lanthen (s154070)
  Scenario: Delete existing merchant
    Given a valid merchant
    When I delete the merchant
    Then the merchant should be deleted

  Scenario: Delete non existing merchant
    Given an invalid merchant
    When I delete the merchant
    Then I should receive an error


  # Author: Lasse Spindler Mørk (s154235)
  Scenario: Read existing merchant
    Given a valid merchant
    When I request merchant information
    Then the merchant should be given

  Scenario: Read non existing merchant
    Given an invalid merchant
    When I request merchant information
    Then I should receive an error

