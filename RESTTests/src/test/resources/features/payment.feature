Feature: Merchant Gateway: Pay
  # Author: Mathies Svarrer-Lanthen (s154070)
  Scenario Outline: Making a transaction with enough money
    Given a valid customer with <c> money and 1 tokens
    And a valid merchant with <m> money
    When a transaction of <amount> is made
    Then a transaction should happen
    And customer account should have <cNew> money
    And merchant account should have <mNew> money

    Examples:
      | c  | m  | amount | cNew | mNew |
      | 1  | 0  | 1      | 0    | 1    |
      | 10 | 10 | 10     | 0    | 20   |
      | 10 | 10 | 5      | 5    | 15   |

  # Author: Mathies Svarrer-Lanthen (s154070)
  Scenario Outline: Making a transaction with not enough money
    Given a valid customer with <c> money and 1 tokens
    And a valid merchant
    When a transaction of <amount> is made
    Then a transaction should not happen

    Examples:
      | c  | amount |
      | 10 | 0      |
      | 10 | -5     |
      | 10 | -15    |
      | 0  | 1      |
      | 10 | 11     |
      | 10 | 20     |

  Scenario: Making a transaction with an invalid merchant
    Given a valid customer with 1 money and 1 tokens
    And an invalid merchant
    When a transaction of 1 is made
    Then a transaction should not happen