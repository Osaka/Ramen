Feature: Customer management
  # Author: Mathies Svarrer-Lanthen (s154070)
  Scenario: Create new customer
    When a new customer is created
    Then a new customer should be created


  # Author: Alexander Armstrong
  Scenario: Deleting existing customers
    Given a valid customer with 0 money
    When I delete that customer
    Then the customer should be deleted

  Scenario: Deleting non existent customers
    And an invalid customer
    When I delete that customer
    Then I should receive an error


  # Author: Mathies Svarrer-Lanthen (s154070)
  Scenario: Updating existing customer
    Given a valid customer with 0 money
    When updating the all customer information
    Then the customer information should be updated

  Scenario: Updating nonexistent customer
    Given an invalid customer
    When updating the all customer information
    Then I should receive an error


  # Author: TODO
  Scenario Outline: Requesting <amount> tokens
    Given a valid customer with 0 money and <x> tokens
    When customer requests <amount> tokens
    Then token request should return <tokens> tokens

    Examples:
      | amount | x | tokens |
      | 4      | 0 | 4      |
      | 1      | 0 | 1      |
      | 1      | 1 | 1      |
      | 6      | 0 | 6      |

  Scenario Outline: Requesting an invalid amount of tokens
    Given a valid customer with 0 money and <x> tokens
    When customer requests <amount> tokens
    Then I should receive an error

    Examples:
      |  x | amount |
      |  2 |      1 |
      | -1 |      0 |
      |  1 |      0 |
      |  4 |      0 |

  Scenario Outline: Get existing tokens
    Given a valid customer with 0 money and <x> tokens
    When customer gets tokens
    Then tokens should match our customer

    Examples:
      | x | tokens |
      | 1 | 1      |
      | 0 | 0      |
      | 2 | 2      |
      | 3 | 3      |
      | 4 | 4      |
      | 5 | 5      |


  # Author: TODO
  Scenario: Getting a customer who exists
    Given a valid customer with 0 money
    When I get the customer
    Then I should receive the customer

  Scenario: Getting a customer who does not exist
    Given an invalid customer
    When I get the customer
    Then I should receive an error