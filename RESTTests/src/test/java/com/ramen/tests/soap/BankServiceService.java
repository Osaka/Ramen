
package com.ramen.tests.soap;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.8-hudson-8-
 * Generated source version: 2.1
 * 
 */
@WebServiceClient(name = "BankServiceService", targetNamespace = "http://fastmoney.ws.dtu/", wsdlLocation = "http://02267-kolkata.compute.dtu.dk:8080/BankService?wsdl")
public class BankServiceService
    extends Service
{

    private final static URL BANKSERVICESERVICE_WSDL_LOCATION;
    private final static Logger logger = Logger.getLogger(com.ramen.tests.soap.BankServiceService.class.getName());

    static {
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = com.ramen.tests.soap.BankServiceService.class.getResource(".");
            url = new URL(baseUrl, "http://02267-kolkata.compute.dtu.dk:8080/BankService?wsdl");
        } catch (MalformedURLException e) {
            logger.warning("Failed to create URL for the wsdl Location: 'http://02267-kolkata.compute.dtu.dk:8080/BankService?wsdl', retrying as a local file");
            logger.warning(e.getMessage());
        }
        BANKSERVICESERVICE_WSDL_LOCATION = url;
    }

    public BankServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public BankServiceService() {
        super(BANKSERVICESERVICE_WSDL_LOCATION, new QName("http://fastmoney.ws.dtu/", "BankServiceService"));
    }

    /**
     * 
     * @return
     *     returns BankService
     */
    @WebEndpoint(name = "BankServicePort")
    public BankService getBankServicePort() {
        return super.getPort(new QName("http://fastmoney.ws.dtu/", "BankServicePort"), BankService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns BankService
     */
    @WebEndpoint(name = "BankServicePort")
    public BankService getBankServicePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://fastmoney.ws.dtu/", "BankServicePort"), BankService.class, features);
    }

}
