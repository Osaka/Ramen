package com.ramen.tests.resttests.cucumber;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import static com.ramen.tests.resttests.cucumber.CucumberRunnerTest.customerGateway;
import static com.ramen.tests.resttests.cucumber.State.*;
import static com.ramen.tests.resttests.cucumber.Utils.getRequest;
import static com.ramen.tests.resttests.cucumber.Utils.postRequest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@Component
public class TokenSteps {

    /**
     * @author Alexander Armstrong
     * @param amount of tokens requested
     * @throws JSONException
     */
    @When("^customer requests (-?\\d+) tokens$")
    public void customerRequestsTokens(int amount) throws JSONException {
        JSONObject tokenRequest = new JSONObject();
        tokenRequest.put("customerId", customer.id);
        tokenRequest.put("amount", amount);
        response = postRequest(customerGateway.getAddress(), "customer/token/", tokenRequest.toString());
    }

    /**
     * @author Alexander Armstrong
     */
    @When("^customer gets tokens$")
    public void customerGetsTokens() {
        response = getRequest(customerGateway.getAddress(), String.format("customer/token/%s", customer.id));
    }

    /**
     * @author Alexander Armstrong
     * @param amount of tokens requested
     */
    @Then("^token request should return (\\d+) tokens$")
    public void tokenRequestShouldReturnTokens(int amount) {
        readTokens();
        assertEquals(amount, tokens.length);
    }

    @Then("^tokens should match our customer$")
    public void tokensShouldMatchOurCustomer() {
        readTokens();
        for (Token token : tokens)
            assertThat(customer.id, is(equalTo(token.ownerId)));
    }

    @Then("^an error should be returned on token request$")
    public void tokenRequestGaveError() {
        Utils.assertResponseCode(400, response.readEntity(String.class));
    }

    private void readTokens() {
        String responseTokens;
        try {
            responseTokens = response.readEntity(String.class);
            assertThat(responseTokens, response.getStatus(), is(equalTo(200)));
        } catch (Exception ignored) {
            return;
        }
        tokens = gson.fromJson(responseTokens, Token[].class);
    }

}
