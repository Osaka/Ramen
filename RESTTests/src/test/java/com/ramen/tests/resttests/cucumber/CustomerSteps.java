    package com.ramen.tests.resttests.cucumber;

    import com.ramen.tests.soap.BankServiceException_Exception;
    import cucumber.api.java.en.And;
    import cucumber.api.java.en.Given;
    import cucumber.api.java.en.Then;
    import cucumber.api.java.en.When;
    import gherkin.deps.com.google.gson.JsonObject;
    import org.hamcrest.MatcherAssert;
    import org.hamcrest.Matchers;
    import org.json.JSONException;
    import org.json.JSONObject;
    import org.junit.Assert;
    import org.springframework.stereotype.Component;

    import javax.ws.rs.core.Response;
    import java.math.BigDecimal;
    import java.util.UUID;

    import static com.ramen.tests.resttests.cucumber.CucumberRunnerTest.customerGateway;
    import static com.ramen.tests.resttests.cucumber.State.*;
    import static com.ramen.tests.resttests.cucumber.Utils.*;
    import static org.hamcrest.CoreMatchers.equalTo;
    import static org.hamcrest.CoreMatchers.is;
    import static org.hamcrest.MatcherAssert.assertThat;
    import static org.junit.Assert.*;

    @Component
    public class CustomerSteps {

        private String newFirstName = "newFirstName";
        private String newLastName = "newLastName";
        private String newAccountNumber = UUID.randomUUID().toString();

        /**
         * @author Mathies Svarrer-Lanthen (s154070)
         */
        @When("^a new customer is created$")
        public void aNewCustomerIsCreated() {
            JsonObject createRequest = new JsonObject();
            createRequest.addProperty("firstName",newFirstName);
            createRequest.addProperty("lastName", newLastName);
            createRequest.addProperty("accountNumber", newAccountNumber);

            response = postRequest(customerGateway.getAddress(), "customer", gson.toJson(createRequest));
        }

        /**
         * @author Mathies Svarrer-Lanthen (s154070)
         */
        @Then("^a new customer should be created$")
        public void aNewCustomerShouldBeCreated() {
            // Check that update was successful
            assertEquals(200, response.getStatus());

            // Check that the information is updated correctly
            AbstractUser newCustomer = gson.fromJson(response.readEntity(String.class), AbstractUser.class);
            assertEquals(newFirstName, newCustomer.firstName);
            assertEquals(newLastName, newCustomer.lastName);
            assertEquals(newAccountNumber, newCustomer.accountNumber);
        }

        /**
         * @author Mathies Svarrer-Lanthen (s154070)
         */
        @Given("^a valid customer with (\\d+) money and (-?\\d+) tokens$")
        public void validCustomerWithMoney(int cash, int amount)
                throws BankServiceException_Exception {
            validCustomerWithMoney(cash);
            JsonObject tokenRequest = new JsonObject();
            tokenRequest.addProperty("customerId", customer.id.toString());
            tokenRequest.addProperty("amount", amount);
            Response tokenResponse = postRequest(customerGateway.getAddress(), "customer/token/", tokenRequest.toString());
            if(amount > 0) {
                tokens = gson.fromJson(tokenResponse.readEntity(String.class), Token[].class);
            }
        }

        /**
         * @author Mathies Svarrer-Lanthen (s154070)
         */
        @Given("^a valid customer with (\\d+) money$")
        public void validCustomerWithMoney(int cash) throws BankServiceException_Exception {
            customer = new AbstractUser();
            customer.cpr = (Utils.generateRandomString(9));
            customer.firstName = ("Jon");
            customer.lastName = ("Jon");
            customer.accountNumber = directBank.createAccountWithBalance(customer.getUser(), BigDecimal.valueOf(cash));

            Response response = postRequest(customerGateway.getAddress(), "customer", gson.toJson(customer));
            String out = response.readEntity(String.class);
            Assert.assertThat(out, response.getStatus(), is(equalTo(200)));

            customer.id = gson.fromJson(out, AbstractUser.class).id;
        }

        @Given("^an invalid customer$")
        public void anInvalidCustomerWithMoney() {
            customer = new AbstractUser();
            customer.id = UUID.randomUUID();
            customer.cpr = Utils.generateRandomString(10);
            customer.accountNumber = UUID.randomUUID().toString();

            Response response = getRequest(customerGateway.getAddress(), String.format("customer/%s", customer.id));
            assertThat("Customer should not exist in DTUPay", response.getStatus(), is(equalTo(400)));

            try {
                directBank.getAccount(customer.accountNumber);
                fail("Customer should not exist in bank");
            } catch (BankServiceException_Exception ignored) {
            }
        }

        /**
         * @author Alexander Armstrong
         */
        @When("^I delete that customer$")
        public void iDeleteThatCustomer() {
            response = Utils.deleteRequest(
                    customerGateway.getAddress(),
                    "customer/" + customer.id);
        }

        @Then("^the customer should be deleted$")
        public void theCustomerShouldBeDeleted() {
            MatcherAssert.assertThat(response.getStatus(), is(equalTo(200)));
        }

        @When("^I get the customer$")
        public void iGetTheCustomer() {
            response = Utils.getRequest(
                    customerGateway.getAddress(),
                    "customer/" + customer.id);
        }

        @Then("^I should receive the customer$")
        public void iShouldReceiveTheCustomer() {
            String body = response.readEntity(String.class);
            AbstractUser customer = gson.fromJson(body, AbstractUser.class);

            MatcherAssert.assertThat(customer.id, is(equalTo(customer.id)));
            MatcherAssert.assertThat(response.getStatus(), is(equalTo(200)));
        }

        /**
         * @author Mathies Svarrer-Lanthen (s154070)
         */
        @When("^updating the all customer information$")
        public void updatingTheAllCustomerInformation() throws JSONException {
            JSONObject json = new JSONObject();
            json.put("id", customer.id);
            json.put("firstName", newFirstName);
            json.put("lastName", newLastName);
            json.put("accountNumber", newAccountNumber);

            response = putRequest(customerGateway.getAddress(), "customer", json.toString());
        }

        /**
         * @author Mathies Svarrer-Lanthen (s154070)
         */
        @Then("^the customer information should be updated$")
        public void theInformationShouldBeUpdated() {
            // Check that update was successful
            assertEquals(200, response.getStatus());

            // Check that the information is updated correctly
            AbstractUser customer = gson.fromJson(response.readEntity(String.class), AbstractUser.class);
            assertEquals(newFirstName, customer.firstName);
            assertEquals(newLastName, customer.lastName);
            assertEquals(newAccountNumber, customer.accountNumber);
        }

        /**
         * @author Mathies Svarrer-Lanthen (s154070)
         */
        @And("^customer account should have (\\d+) money$")
        public void customerAccountShouldHaveCNewMoney(int amount) throws BankServiceException_Exception {
            BigDecimal actual = BigDecimal.valueOf(amount).stripTrailingZeros();
            BigDecimal expected = directBank.getAccount(customer.accountNumber).getBalance().stripTrailingZeros();
            assertThat(actual, is(Matchers.equalTo(expected)));
        }
    }
