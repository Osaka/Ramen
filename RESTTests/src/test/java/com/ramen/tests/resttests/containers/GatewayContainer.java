package com.ramen.tests.resttests.containers;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;

/**
 * @author Morten Telling
 */
public class GatewayContainer extends GenericContainer {

    private int port;

    public GatewayContainer(String name, int port, Network network) {
        super(name + ":" + Config.getTag());
        this.port = port;
        this.withEnv(Config.env);
        this.withEnv("RABBITMQ_HOST", "rabbitmq");
        this.withEnv("RABBITMQ_PORT", "5672");
        this.withNetwork(network);
        this.withExposedPorts(port);
    }

    public String getAddress() {
        return "http://" + this.getContainerIpAddress() + ":" + this.getMappedPort(this.port);
    }

}
