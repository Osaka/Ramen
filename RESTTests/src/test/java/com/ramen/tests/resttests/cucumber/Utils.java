package com.ramen.tests.resttests.cucumber;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Random;

import static com.ramen.tests.resttests.cucumber.State.response;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Utils {
    public Utils() {
    }

    static Response putRequest(String base, String path, String message) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(base);
        return target.path(path).request()
                .accept(MediaType.APPLICATION_JSON)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .put(Entity.entity(message, MediaType.APPLICATION_JSON));
    }

    static Response postRequest(String base, String path, String message) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(base);
        return target.path(path).request()
                .accept(MediaType.APPLICATION_JSON)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .post(Entity.entity(message, MediaType.APPLICATION_JSON));
    }

    static Response getRequest(String base, String path) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(base);
        return target.path(path).request().get();
    }

    static Response deleteRequest(String base, String path) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(base);
        return target.path(path).request().delete();
    }

    // From https://www.baeldung.com/java-random-string
    static String generateRandomString(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    static void assertResponseCode(int isThis) {
        assertResponseCode(isThis, "");
    }

    static void assertResponseCode(int isThis, String error) {
        assertThat(error, response.getStatus(), is(equalTo(isThis)));
    }
}