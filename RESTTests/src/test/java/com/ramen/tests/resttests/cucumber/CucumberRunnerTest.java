package com.ramen.tests.resttests.cucumber;

import com.ramen.tests.resttests.containers.Config;
import com.ramen.tests.resttests.containers.GatewayContainer;
import com.ramen.tests.resttests.containers.ServiceContainer;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.output.Slf4jLogConsumer;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author Morten Telling
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = "com.ramen.tests.resttests.cucumber",
        format = {"pretty",
                "json:target/cucumber.json"})
public class CucumberRunnerTest {

    public static GatewayContainer customerGateway, merchantGateway;

    static {
        Logger logger = LoggerFactory.getLogger("org.testcontainers");
        Slf4jLogConsumer logConsumer = new Slf4jLogConsumer(logger);

        Network network = Network.newNetwork();

        GenericContainer tokenDB =
                new GenericContainer("postgres")
                        .withEnv(Config.env)
                        .withNetwork(network)
                        .withNetworkAliases("token_db")
                        .withEnv("POSTGRES_DB", "token_db")
                        .withStartupTimeout(Duration.of(180, ChronoUnit.SECONDS));
        tokenDB.start();

        GenericContainer merchantDB =
                new GenericContainer("postgres")
                        .withEnv(Config.env)
                        .withNetwork(network)
                        .withNetworkAliases("merchant_db")
                        .withEnv("POSTGRES_DB", "merchant_db")
                        .withStartupTimeout(Duration.of(180, ChronoUnit.SECONDS));
        merchantDB.start();

        GenericContainer customerDB =
                new GenericContainer("postgres")
                        .withEnv(Config.env)
                        .withNetwork(network)
                        .withNetworkAliases("customer_db")
                        .withEnv("POSTGRES_DB", "customer_db")
                        .withStartupTimeout(Duration.of(180, ChronoUnit.SECONDS));
        customerDB.start();

        GenericContainer transactionDB =
                new GenericContainer("postgres")
                        .withEnv(Config.env)
                        .withNetwork(network)
                        .withNetworkAliases("transaction_db")
                        .withEnv("POSTGRES_DB", "transaction_db")
                        .withStartupTimeout(Duration.of(180, ChronoUnit.SECONDS));
        transactionDB.start();

        GenericContainer rabbitmq =
                new GenericContainer("rabbitmq:3")
                        .withEnv(Config.env)
                        .withNetwork(network)
                        .withNetworkAliases("rabbitmq")
                        .withStartupTimeout(Duration.of(180, ChronoUnit.SECONDS));
        rabbitmq.start();

        ServiceContainer paymentService =
                new ServiceContainer("ramen_payment", network, "");
        paymentService.withLogConsumer(logConsumer);
        paymentService.start();

        ServiceContainer bankService =
                new ServiceContainer("ramen_bank", network, "");
        bankService.withLogConsumer(logConsumer);
        bankService.start();

        merchantGateway =
                new GatewayContainer("ramen_merchant_gateway", 8080, network);
        merchantGateway.start();

        customerGateway =
                new GatewayContainer("ramen_customer_gateway", 8080, network);
        customerGateway.start();

        ServiceContainer customerService =
                new ServiceContainer("ramen_customer", network, "customer_db");
        customerService.withLogConsumer(logConsumer);
        customerService.start();

        ServiceContainer merchantService =
                new ServiceContainer("ramen_merchant", network, "merchant_db");
        merchantService.withLogConsumer(logConsumer);
        merchantService.start();

        ServiceContainer tokenService =
                new ServiceContainer("ramen_token", network, "token_db");
        tokenService.withLogConsumer(logConsumer);
        tokenService.start();

        ServiceContainer transactionService =
                new ServiceContainer("ramen_transaction", network, "transaction_db");
        transactionService.withLogConsumer(logConsumer);
        transactionService.start();

        try {
            paymentService.waitForRabbitMq();
            bankService.waitForRabbitMq();
            customerService.waitForRabbitMq();
            merchantService.waitForRabbitMq();
            tokenService.waitForRabbitMq();
            transactionService.waitForRabbitMq();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
}
