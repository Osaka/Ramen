package com.ramen.tests.resttests.cucumber;

import com.ramen.tests.soap.BankService;
import com.ramen.tests.soap.BankServiceService;
import com.ramen.tests.soap.User;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import gherkin.deps.com.google.gson.Gson;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.UUID;

public class State {

    @Before
    public void before() {
        System.out.println("Before");
        merchant = null;
        customer = null;
        response = null;
        tokens = null;
        specialTestData = new HashMap<>();
        transactionReport = null;
    }

    @After
    public void after() {
        System.out.println("After");
        // Delete customer account in bank
        try {
            directBank.retireAccount(customer.accountNumber);
        } catch (Exception ignored) {
        }
        // Delete merchant account in bank
        try {
            directBank.retireAccount(merchant.accountNumber);
        } catch (Exception ignored) {
        }
    }

    public static final Gson gson = new Gson();

    public static final BankService directBank = new BankServiceService().getBankServicePort();

    public static AbstractUser merchant;

    public static AbstractUser customer;

    public static HashMap<String, Object> specialTestData = new HashMap<>();

    public static Response response;

    public static Token[] tokens;

    public static Transaction[] transactionReport;


    @Then("^I should receive a success$")
    public void iShouldReceiveSuccess() {
        String resp = response.readEntity(String.class);
        Utils.assertResponseCode(200, resp);
    }

    @Then("^I should receive an error$")
    public void iShouldReceiveAnError() {
        String resp = response.readEntity(String.class);
        Utils.assertResponseCode(400, resp);
    }

}

class Token {
    UUID ownerId;
    UUID id;
    String url;
}

class Transaction {
    UUID id;
    UUID customerId;
    UUID merchantId;
    UUID tokenId;
    BigDecimal amount;
    Boolean refunded;
}

class AbstractUser {
    UUID id;
    String accountNumber;
    String cpr, firstName, lastName;

    User getUser() {
        User user = new User();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setCprNumber(cpr);
        return user;
    }
}