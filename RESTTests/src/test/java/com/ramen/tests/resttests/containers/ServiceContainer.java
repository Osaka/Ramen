package com.ramen.tests.resttests.containers;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.output.WaitingConsumer;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.testcontainers.containers.output.OutputFrame.OutputType.STDOUT;

/**
 * @author Morten Telling
 */
public class ServiceContainer extends GenericContainer {

    public ServiceContainer(String name, Network network, String db) {
        super(name + ":" + Config.getTag());

        this.withEnv(Config.env);
        this.withEnv("RABBITMQ_HOST", "rabbitmq");
        this.withEnv("RABBITMQ_PORT", "5672");
        this.withEnv("POSTGRES_PORT", "5432");
        this.withEnv("POSTGRES_HOST", db);
        this.withEnv("POSTGRES_DB", db);
        this.withNetwork(network);
    }

    public void waitForRabbitMq() throws TimeoutException {
        WaitingConsumer consumer = new WaitingConsumer();

        this.followOutput(consumer, STDOUT);

        consumer.waitUntil(frame ->
                frame.getUtf8String().contains("Created new connection: rabbitConnectionFactory"), 120, TimeUnit.SECONDS);

    }
}
