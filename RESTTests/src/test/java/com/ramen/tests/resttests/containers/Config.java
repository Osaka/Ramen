package com.ramen.tests.resttests.containers;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Morten Telling
 */
public class Config {
    public static final Map<String, String> env = new HashMap<String, String>() {{
        put("POSTGRES_USER", "test");
        put("POSTGRES_PASSWORD", "test");
        put("RABBITMQ_DEFAULT_USER", "test");
        put("RABBITMQ_DEFAULT_PASS", "test");
    }};

    public static String getTag() {
        String tag = System.getenv("BRANCH_NAME");
        if (tag == null)
            tag = "latest";

        return tag;
    }

}
