package com.ramen.tests.resttests.cucumber;

import com.ramen.tests.soap.BankServiceException_Exception;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.JsonObject;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.UUID;

import static com.ramen.tests.resttests.cucumber.CucumberRunnerTest.merchantGateway;
import static com.ramen.tests.resttests.cucumber.State.*;
import static com.ramen.tests.resttests.cucumber.Utils.*;
import static java.math.BigDecimal.valueOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * @author Lasse Spindler Mørk (s154235)
 */
public class MerchantSteps {

    private String newAccountNumber = UUID.randomUUID().toString();

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @When("^a new merchant is created$")
    public void aMerchantIsCreated() {
        JsonObject createRequest = new JsonObject();
        createRequest.addProperty("accountNumber", newAccountNumber);

        response = postRequest(merchantGateway.getAddress(), "merchant", createRequest.toString());
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @Then("^a new merchant should be created$")
    public void aNewMerchantShouldBeCreated() {
        // Check that update was successful
        assertEquals(200, response.getStatus());

        // Check that the information is correct
        AbstractUser newMerchant = gson.fromJson(response.readEntity(String.class), AbstractUser.class);
        assertEquals(newAccountNumber, newMerchant.accountNumber);
    }

    @Given("^a valid merchant$")
    public void validMerchant() throws BankServiceException_Exception {
        validMerchantWithMoney(0);
    }

    @Given("^a valid merchant with (\\d+) money$")
    public void validMerchantWithMoney(int cash) throws BankServiceException_Exception {
        merchant = new AbstractUser();
        merchant.cpr = (Utils.generateRandomString(9));
        merchant.firstName = ("John");
        merchant.lastName = ("John");
        merchant.accountNumber = directBank.createAccountWithBalance(merchant.getUser(), valueOf(cash));

        JsonObject body = new JsonObject();
        body.addProperty("accountNumber", merchant.accountNumber);

        Response response = postRequest(merchantGateway.getAddress(), "merchant", body.toString());
        String out = response.readEntity(String.class);
        assertThat(out, response.getStatus(), is(equalTo(200)));

        merchant.id = gson.fromJson(out, AbstractUser.class).id;
    }

    @Given("^an invalid merchant$")
    public void anInvalidMerchant() {
        merchant = new AbstractUser();
        merchant.id = UUID.randomUUID();
        merchant.cpr = Utils.generateRandomString(10);
        merchant.accountNumber = UUID.randomUUID().toString();

        Response response = getRequest(merchantGateway.getAddress(), String.format("merchant/%s", merchant.id));
        assertThat("Merchant should not exist in DTUPay", response.getStatus(), is(equalTo(400)));

        try {
            directBank.getAccount(merchant.accountNumber);
            fail("Merchant should not exist in bank");
        } catch (BankServiceException_Exception ignored) {
        }
    }

    @When("^I request merchant information$")
    public void theMerchantIsRequested() {
        response = getRequest(merchantGateway.getAddress(), String.format("merchant/%s", merchant.id));
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @When("^I delete the merchant$")
    public void theMerchantIsDeleted() {
        response = deleteRequest(merchantGateway.getAddress(), String.format("merchant/%s", merchant.id));
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @When("^I update the merchant information$")
    public void theMerchantIsUpdated() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", merchant.id);
        json.put("accountNumber", newAccountNumber);
        response = putRequest(merchantGateway.getAddress(), "merchant", json.toString());
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @Then("^the merchant should be deleted$")
    public void theMerchantShouldBeDeleted() {
        String resp = response.readEntity(String.class);
        Utils.assertResponseCode(200, resp);
    }

    @Then("^the merchant should be given$")
    public void theMerchantShouldBeGiven() {
        String resp = response.readEntity(String.class);
        Utils.assertResponseCode(200, resp);
        AbstractUser receivedMerchant = gson.fromJson(resp, AbstractUser.class);

        assertThat(receivedMerchant.id, is(equalTo(merchant.id)));
        assertThat(receivedMerchant.accountNumber, is(equalTo(merchant.accountNumber)));
    }

    @Then("^merchant account should have (\\d+) money$")
    public void merchantAccountShouldHaveMNewMoney(int amount) throws BankServiceException_Exception {
        BigDecimal actual = BigDecimal.valueOf(amount).stripTrailingZeros();
        BigDecimal expected = directBank.getAccount(merchant.accountNumber).getBalance().stripTrailingZeros();
        MatcherAssert.assertThat(actual, is(Matchers.equalTo(expected)));
    }

    @Then("^the merchant information should be updated$")
    public void theMerchantShouldBeUpdated() {
        // Check that update was successful
        assertEquals(200, response.getStatus());

        // Check that the information is updated correctly
        AbstractUser merchant = gson.fromJson(response.readEntity(String.class), AbstractUser.class);
        assertEquals(newAccountNumber, merchant.accountNumber);
    }
}
