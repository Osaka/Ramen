package com.ramen.tests.resttests.cucumber;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;

import javax.ws.rs.core.Response;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static com.ramen.tests.resttests.cucumber.CucumberRunnerTest.customerGateway;
import static com.ramen.tests.resttests.cucumber.CucumberRunnerTest.merchantGateway;
import static com.ramen.tests.resttests.cucumber.State.*;
import static com.ramen.tests.resttests.cucumber.Utils.getRequest;
import static com.ramen.tests.resttests.cucumber.Utils.putRequest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PaymentSteps {

    private String transactionId;

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @When("^a transaction of (-?\\d+) is made$")
    public void aTransactionOfAmountIsMade(int amount) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("merchantId", merchant.id);
        json.put("tokenId", tokens[0].id);
        json.put("amount", amount);

        response = putRequest(merchantGateway.getAddress(), "merchant/payment", json.toString());
        transactionId = response.readEntity(String.class);
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @Then("^a transaction should happen$")
    public void aTransactionShouldHappen() {
        int statusCodeReturned = response.getStatus();
        assertThat(transactionId, statusCodeReturned, is(equalTo(200)));
//        try {
//            transactionId = UUID.fromString(responseString);
//        } catch (Exception e) {
//            fail("Could not parse transaction id " + responseString);
//        }
    }

    /**
     * @author Mathies Svarrer-Lanthen (s154070)
     */
    @Then("^a transaction should not happen$")
    public void aTransactionShouldNotHappen() {
        assertEquals(400, response.getStatus());
    }

    @When("^a refund is requested$")
    public void aRefundIsRequested() {
        String json = gson.toJson(transactionId);
        Response resp = putRequest(merchantGateway.getAddress(), "merchant/transaction", json);

        Assert.assertThat(resp.readEntity(String.class), resp.getStatus(), is(Matchers.equalTo(200)));
    }

    @When("^a customer ask for a transaction report log between today and tomorrow$")
    public void aCustomerAskForATransactionReportLog() {
        OffsetDateTime toTime = OffsetDateTime.now().plusDays(1);
        OffsetDateTime fromTime = toTime.minusDays(5);
        String path = String.format("customer/transaction/%s/%s/%s",
                customer.id, String.valueOf(fromTime.toEpochSecond()), String.valueOf(toTime.toEpochSecond()));
        response = getRequest(customerGateway.getAddress(), path);
    }

    /**
     * @author Mathias E. Asmussen - s154219
     */
    @When("^a merchant ask for a transaction report log between today and tomorrow$")
    public void aMerchantAskForATransactionReportLog() {
        OffsetDateTime toTime = OffsetDateTime.now(ZoneOffset.UTC).plusDays(1);
        OffsetDateTime fromTime = toTime.minusDays(5);
        String path = String.format("merchant/transaction/%s/%s/%s",
                merchant.id, String.valueOf(fromTime.toEpochSecond()),
                String.valueOf(toTime.toEpochSecond()));
        response = getRequest(merchantGateway.getAddress(), path);
    }

    /**
     * @author Mathias E. Asmussen - s154219
     */
    @Then("^a transaction log of length (\\d+) should be returned$")
    public void aTransactionLogOfLengthShouldBeReturned(int expectedLength) {
        int statusCode = response.getStatus();
        String body = response.readEntity(String.class);
        assertThat(body, statusCode, is(Matchers.equalTo(200)));

        try {
            transactionReport = gson.fromJson(body, Transaction[].class);
            assertThat(transactionReport.length, is(equalTo(expectedLength)));
        } catch (Exception e) {
            fail("Could not parse transaction report");
        }
    }

    /**
     * @author Mathias E. Asmussen - s154219
     */
    @And("^that transaction should contain the customer token$")
    public void thatTransactionShouldContainTheCustomerToken() {
        if (transactionReport.length > 0) {
            assertThat(transactionReport[0].tokenId, is(equalTo(tokens[0].id)));
        } else {
            fail("TransactionReport is empty");
        }
    }

    @And("^that transaction should have an amount of (\\d+) money$")
    public void thatTransactionShouldHaveAnAmountOfMoney(int expectedMoney) {
        BigDecimal expectedAmount = new BigDecimal(expectedMoney, new MathContext(2));
        if (transactionReport.length > 0) {
            assertThat(transactionReport[0].amount, is(Matchers.comparesEqualTo(expectedAmount)));
        } else {
            fail("TransactionReport is empty");
        }
    }

}
